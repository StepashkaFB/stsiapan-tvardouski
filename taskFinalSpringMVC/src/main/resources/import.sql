insert into USER(password, role, username) values('$2a$12$fVMVXd/siYiLigM1O8Ix/uGwn4OEjxM6dzhqYfG9kyWXKQuRdro3e','ROLE_USER','yy');
insert into USER(password, role, username) values('$2a$12$fVMVXd/siYiLigM1O8Ix/uGwn4OEjxM6dzhqYfG9kyWXKQuRdro3e','ROLE_ADMIN','tt');
insert into category(nameofcategory) values('fruit'),('vegetable'),('berries'),('flower');
insert into PRODUCT(AMOUNTOFPRODUCT, FILENAME, NAMEOFPRODUCT, DESCRIPTION, PRICE, CATEGORY_ID) values(10,'rose.jpg','Rose','Color red',100,4);
insert into PRODUCT(AMOUNTOFPRODUCT, FILENAME, NAMEOFPRODUCT, DESCRIPTION, PRICE, CATEGORY_ID) values(15,'banana.jpg','Banana','A banana is an edible fruit – botanically a berry – produced by several kinds of large herbaceous flowering plants in the genus Musa',103,1);


