package com.epam.exceptions;

public class CategoryExistException extends ExistException {
    public CategoryExistException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public CategoryExistException(String errorMessage) {
        super(errorMessage);
    }
}
