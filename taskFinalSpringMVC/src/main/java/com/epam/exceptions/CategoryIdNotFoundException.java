package com.epam.exceptions;

public class CategoryIdNotFoundException extends IdNotFoundException {
    public CategoryIdNotFoundException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public CategoryIdNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
