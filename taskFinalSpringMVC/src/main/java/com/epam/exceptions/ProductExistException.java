package com.epam.exceptions;

public class ProductExistException extends ExistException {
    public ProductExistException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public ProductExistException(String errorMessage) {
        super(errorMessage);
    }
}
