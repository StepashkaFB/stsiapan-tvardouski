package com.epam.exceptions;

public class ProductIdNotFoundException extends IdNotFoundException {
    public ProductIdNotFoundException(String errorMessage, Throwable throwable) {
        super(errorMessage, throwable);
    }

    public ProductIdNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
