package com.epam.exceptions;

public class ExistException extends RuntimeException {
    public ExistException(String errorMessage, Throwable throwable){
        super(errorMessage,throwable);
    }
    public ExistException(String errorMessage){
        super(errorMessage);
    }
}
