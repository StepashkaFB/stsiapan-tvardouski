package com.epam.exceptions;

public class IdNotFoundException extends RuntimeException {
    public IdNotFoundException(String errorMessage, Throwable throwable){
        super(errorMessage,throwable);
    }
    public IdNotFoundException(String errorMessage){
        super(errorMessage);
    }
}
