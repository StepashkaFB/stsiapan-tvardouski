package com.epam.controller;

import com.epam.model.User;
import com.epam.services.UserService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Log4j
@Controller
public class RegistrationController {
    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign_up")
    public String registrationUser(@ModelAttribute User user, Model model) {
        if (userService.getUserByName(user) != null) {
            String exists = "User already exist with name: " + user.getUsername();
            log.info(exists);
            model.addAttribute("error", exists);
            return "registration";
        }
        userService.saveUser(user);
        return "redirect:/login";
    }

    @GetMapping("/reg")
    public String getRegistration() {
        return "registration";
    }
}
