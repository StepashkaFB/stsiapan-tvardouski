package com.epam.controller;

import com.epam.exceptions.ExistException;
import com.epam.exceptions.IdNotFoundException;
import com.epam.model.Category;
import com.epam.model.Product;
import com.epam.services.CategoryService;
import com.epam.services.ProductService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

@Log4j
@Controller
@RequestMapping("/admin")
public class AdminController {
    private static final String PRODUCT = "product";
    private static final String CATEGORY = "category";
    private static final String PRODUCTS = "products";
    private static final String CATEGORIES = "categories";
    private static final String ID = "id";
    private static final String CATEGORY_PRODUCT = "cat";
    private static final String ID_CAT = "idCat";
    private static final String FILE = "file";
    private static final String ERROR = "error";
    private static final String ERRORS_PAGE_NAME = "errorsPages/errorAdd";

    private final ProductService productService;
    private final CategoryService categoryService;

    @Autowired
    public AdminController(ProductService productService, CategoryService categoryService) {
        this.productService = productService;
        this.categoryService = categoryService;
    }

    @GetMapping("/getAll")
    public String getProducts(Model model) {
        model.addAttribute(PRODUCT, new Product());
        model.addAttribute(CATEGORY, new Category());
        model.addAttribute(PRODUCTS, productService.getAll());
        model.addAttribute(CATEGORIES, categoryService.getAll());
        return "admin";
    }

    @GetMapping("/updatePage")
    public String editPage(@RequestParam(ID) String id, Model model) {
        Product product = productService.getProductById(Integer.parseInt(id));
        model.addAttribute(PRODUCT, product);
        model.addAttribute(CATEGORY_PRODUCT, product.getCategory());
        model.addAttribute(CATEGORIES, categoryService.getAll());
        return "update";
    }

    @PostMapping("/updateProduct")
    public String updateProduct(@ModelAttribute Product product,
                                @RequestParam(ID_CAT) Integer idCat,
                                @RequestParam(value = FILE, required = false) MultipartFile file) throws IOException {
        productService.update(product, idCat, file);
        return "redirect:getAll";
    }

    @PostMapping("/deleteProduct")
    public String deleteProduct(@RequestParam(ID) String id) {
        productService.deleteById(Integer.parseInt(id));
        return "redirect:getAll";
    }

    @PostMapping("/addProduct")
    public String addProduct(@RequestParam(value = FILE, required = false) MultipartFile file,
                             @ModelAttribute Product product,
                             @RequestParam(ID_CAT) Integer idCat) throws IOException {
        productService.save(product, idCat, file);
        return "redirect:getAll";
    }


    @PostMapping("/addCategory")
    public String addCategory(@ModelAttribute Category category) {
        categoryService.save(category);
        return "redirect:getAll";
    }

    @ExceptionHandler({ExistException.class, IOException.class, IdNotFoundException.class})
    public ModelAndView handleIOException(RuntimeException exception) {
        ModelAndView modelAndView = new ModelAndView(ERRORS_PAGE_NAME);
        modelAndView.addObject(ERROR, exception.getMessage());
        log.error(exception.getMessage(), exception);
        return modelAndView;
    }
}
