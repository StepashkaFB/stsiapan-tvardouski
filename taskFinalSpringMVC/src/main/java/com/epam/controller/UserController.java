package com.epam.controller;

import com.epam.exceptions.IdNotFoundException;
import com.epam.services.BasketService;
import com.epam.services.CategoryService;
import com.epam.services.ProductService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Log4j
@Controller
@RequestMapping("/user")
public class UserController {
    private static final String ERROR = "error";
    private static final String ERRORS_PAGE_NAME = "errorsPages/errorUser";

    private final ProductService productService;
    private final CategoryService categoryService;
    private final BasketService basketService;

    @Autowired
    public UserController(ProductService productService,
                          CategoryService categoryService,
                          BasketService basketService) {
        this.productService = productService;
        this.categoryService = categoryService;
        this.basketService = basketService;
    }

    @GetMapping("/getProducts")
    public String getProducts(@RequestParam(value = "idCat", defaultValue = "-1")
                                      String idCat, Model model) {
        if (!idCat.equals("-1")) {
            model.addAttribute("products",
                    productService.getProductByIdOfCategory(Integer.parseInt(idCat)));
        } else {
            model.addAttribute("products", productService.getAll());
        }
        model.addAttribute("categories", categoryService.getAll());
        return "user";
    }

    @PostMapping("/addToBasket")
    public String addToCard(@RequestParam("id") Integer id, HttpSession httpSession) {
        basketService.save(id, httpSession.getAttribute("currentUser").toString());
        return "redirect:getProducts";
    }

    @GetMapping("/showBasket")
    public String showBasket(Model model, HttpSession httpSession) {
        String userName = httpSession.getAttribute("currentUser").toString();
        model.addAttribute("baskets", basketService.getByUserName(userName));
        return "basket";
    }

    @PostMapping("/buy")
    public String buyProduct(@RequestParam("id") Integer id,
                             @RequestParam("amount") Integer amount,
                             HttpSession httpSession) {
        basketService.buyAndDeleteProduct
                (id, httpSession.getAttribute("currentUser").toString(), amount);
        return "redirect:showBasket";
    }

    @PostMapping("/delete")
    public String deleteProduct(@RequestParam("id") Integer id, HttpSession httpSession) {
        basketService.delete(id, httpSession.getAttribute("currentUser").toString());
        return "redirect:showBasket";
    }

    @ExceptionHandler({IllegalArgumentException.class, IdNotFoundException.class})
    public ModelAndView handleIOException(RuntimeException exception) {
        ModelAndView modelAndView = new ModelAndView(ERRORS_PAGE_NAME);
        modelAndView.addObject(ERROR, exception.getMessage());
        log.error(exception.getMessage(), exception);
        return modelAndView;
    }
}
