package com.epam.controller;

import lombok.extern.log4j.Log4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpSession;

@Log4j
@SessionAttributes({"currentUser"})
@Controller
public class LoginController {

    private static final String LOGGED_IN_USER = "Logged in, user: ";
    private static final String CURRENT_USER = "currentUser";
    private static final String ERROR = "error";
    private static final String INVALID_LOGIN_OF_PASSWORD = "Invalid login of password";
    private static final int TIME_SESSION = 180;

    @GetMapping("/accessDenied")
    public String accessDenied() {
        return "accessDenied";
    }

    @GetMapping({"/login", "/"})
    public String login() {
        return "login";
    }

    @GetMapping("/loginFailed")
    public String loginError(Model model) {
        model.addAttribute(ERROR, INVALID_LOGIN_OF_PASSWORD);
        return "login";
    }

    @PostMapping("/postLogin")
    public String postLogin(HttpSession session) {
        UsernamePasswordAuthenticationToken authentication
                = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        UserDetails loggedInUser = (UserDetails) authentication.getPrincipal();
        session.setMaxInactiveInterval(TIME_SESSION);
        String username = loggedInUser.getUsername();
        session.setAttribute(CURRENT_USER, username);
        log.info(LOGGED_IN_USER + username);
        return "redirect:/user/getProducts";
    }
}
