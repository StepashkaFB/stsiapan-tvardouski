package com.epam.services;

import com.epam.exceptions.CategoryExistException;
import com.epam.exceptions.CategoryIdNotFoundException;
import com.epam.model.Category;
import com.epam.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryService {

    private static final String CATEGORY_NOT_FOUND_WITH_ID = "Category not found with id: ";
    private static final String EXIST_WITH_NAME = "Exist with name: ";
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Transactional(readOnly = true)
    public List<Category> getAll() {
        return categoryRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Category getCategoryById(int id) {
        return categoryRepository.findById(id)
                .orElseThrow(() -> new CategoryIdNotFoundException(CATEGORY_NOT_FOUND_WITH_ID + id));
    }

    public void save(Category category) {
        String nameOfCategory = category.getNameOfCategory();
        if (categoryRepository.getByNameOfCategory(nameOfCategory) != null) {
            throw new CategoryExistException(EXIST_WITH_NAME + nameOfCategory);
        }
        categoryRepository.save(category);
    }
}
