package com.epam.services;

import com.epam.model.Basket;
import com.epam.model.Product;
import com.epam.repository.BasketRepository;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j
@Service
@Transactional
public class BasketService {
    private static final String PRODUCT_IS_DELETED_FROM_BASKET_WHERE_ID_PRODUCT_AND_USERNAME =
            "Product is deleted from basket, where id product and username: ";
    private static final String INCORRECT_AMOUNT_OF_PRODUCT = "Incorrect amount of product";
    private static final String USER_ADDED_A_PRODUCT_WHICH_ALREADY_EXIST_IN_BASKET =
            " user added a product, which already exist in basket: ";
    private static final String USER_ADDED_A_PRODUCT_TO_BASKET = " user added a product to basket: ";

    private final BasketRepository basketRepository;
    private final ProductService productService;

    @Autowired
    public BasketService(BasketRepository basketRepository,
                         ProductService productService) {
        this.basketRepository = basketRepository;
        this.productService = productService;
    }

    public void save(int idProduct, String userName) {
        Basket temp = basketRepository.getByProduct_IdAndUsername(idProduct, userName);
        if (temp != null) {
            temp.increaseAmount();
            log.info(userName + USER_ADDED_A_PRODUCT_WHICH_ALREADY_EXIST_IN_BASKET + idProduct);
        } else {
            Product product = productService.getProductById(idProduct);
            Basket basket = new Basket();
            basket.increaseAmount();
            basket.setProduct(product);
            basket.setUsername(userName);
            basketRepository.save(basket);
            log.info(userName + USER_ADDED_A_PRODUCT_TO_BASKET + product);
        }
    }

    public void delete(int idProduct, String userName) {
        basketRepository.deleteByProduct_IdAndUsername(idProduct, userName);
        log.info(PRODUCT_IS_DELETED_FROM_BASKET_WHERE_ID_PRODUCT_AND_USERNAME +
                idProduct + " -- " + userName);
    }

    public void buyAndDeleteProduct(int idProduct, String userName, int amount) {
        Basket basket = basketRepository.getByProduct_IdAndUsername(idProduct, userName);
        if (basket == null || basket.getProduct().getAmountOfProduct() < amount) {
            throw new IllegalArgumentException(INCORRECT_AMOUNT_OF_PRODUCT);
        }
        basket.getProduct().decreaseAmount(amount);
        delete(idProduct, userName);
    }

    @Transactional(readOnly = true)
    public List<Basket> getByUserName(String name) {
        return basketRepository.getByUsername(name);
    }
}
