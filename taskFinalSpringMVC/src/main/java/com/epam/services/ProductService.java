package com.epam.services;

import com.epam.exceptions.ProductExistException;
import com.epam.exceptions.ProductIdNotFoundException;
import com.epam.model.Category;
import com.epam.model.Product;
import com.epam.repository.ProductRepository;
import com.epam.util.FileUtil;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Log4j
@Service
@Transactional
public class ProductService {
    private static final String EXIST_WITH_NAME = "Exist with name: ";
    private static final String PRODUCT_IS_SAVED_WITH_NAME = "Product is saved, with name: ";
    private static final String PRODUCT_NOT_FOUND_WITH_ID = "Product not found with id: ";
    private static final String PRODUCT_BEFORE_UPDATE = "Product before update: ";
    private static final String PRODUCT_AFTER_UPDATE = "Product after update: ";
    private static final String PRODUCT_IS_DELETED = "Product is deleted: ";

    private ProductRepository productRepository;
    private CategoryService categoryService;

    @Autowired
    public ProductService(ProductRepository productRepository,
                          CategoryService categoryService) {
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    @Transactional(readOnly = true)
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    public void save(Product product, Integer cat, MultipartFile file) throws IOException {
        final String nameOfProduct = product.getNameOfProduct();
        if (productRepository.getByNameOfProduct(nameOfProduct) != null) {
            throw new ProductExistException(EXIST_WITH_NAME + nameOfProduct);
        }
        finalSave(product, cat, file);
        log.info(PRODUCT_IS_SAVED_WITH_NAME + product);
    }

    private void finalSave(Product product, Integer cat, MultipartFile file) throws IOException {
        Category category = categoryService.getCategoryById(cat);
        product.setFilename(FileUtil.addFile(file));
        product.setCategory(category);
        productRepository.save(product);
    }

    public void update(Product product, Integer cat, MultipartFile file)
            throws IOException {
        Product temp = getProductById(product.getId());
        log.info(PRODUCT_BEFORE_UPDATE + temp);
        FileUtil.deleteFile(temp.getFilename());
        finalSave(product, cat, file);
        log.info(PRODUCT_AFTER_UPDATE + product);
    }

    @Transactional(readOnly = true)
    public Product getProductById(Integer id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ProductIdNotFoundException(PRODUCT_NOT_FOUND_WITH_ID + id));
    }

    @Transactional(readOnly = true)
    public List<Product> getProductByIdOfCategory(Integer id) {
        return productRepository.getByCategory_Id(id);
    }

    public void deleteById(Integer id) {
        Product product = getProductById(id);
        FileUtil.deleteFile(product.getFilename());
        productRepository.deleteById(id);
        log.info(PRODUCT_IS_DELETED + product);
    }
}
