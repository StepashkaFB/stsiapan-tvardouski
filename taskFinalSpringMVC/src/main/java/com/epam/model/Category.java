package com.epam.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


@Getter
@Setter
@ToString(exclude = "products")
@NoArgsConstructor
@Entity
@Table(name = "category")
public class Category implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nameOfCategory;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category")
    private Set<Product> products = new HashSet<>();

    public Category(int id, String nameOfCategory) {
        this.id=id;
        this.nameOfCategory=nameOfCategory;
    }

    public void addProduct(Product product){
        product.setCategory(this);
        products.add(product);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Category)) return false;
        Category category = (Category) o;
        return id.equals(category.id) &&
                nameOfCategory.equals(category.nameOfCategory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameOfCategory);
    }
}
