package com.epam.util;

import lombok.extern.log4j.Log4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private static final String USER = "User: ";
    private static final String ATTEMPTED_TO_ACCESS_THE_PROTECTED_URL = " attempted to access the protected URL: ";
    private static final String ACCESS_DENIED = "/accessDenied";

    @Override
    public void handle(
            HttpServletRequest request,
            HttpServletResponse response,
            AccessDeniedException exc) throws IOException {

        Authentication auth
                = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            log.warn(USER + auth.getName()
                    + ATTEMPTED_TO_ACCESS_THE_PROTECTED_URL
                    + request.getRequestURI());
        }
        response.sendRedirect(request.getContextPath() + ACCESS_DENIED);
    }
}
