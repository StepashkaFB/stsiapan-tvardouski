package com.epam.util;

import lombok.extern.log4j.Log4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static com.epam.constants.MainConstant.UPLOAD_PATH;

@Log4j
public class FileUtil {

    private static final String FILE_IS_DELETED = "File is deleted: ";
    private static final String DELETE_FILE_FAILED = "Delete file failed: ";
    private static final String SLASH = "./";

    private FileUtil() {
    }

    public static String addFile(MultipartFile file) throws IOException {
        String result = "";
        if (file != null) {
            File uploadDir = new File(UPLOAD_PATH);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            result = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(SLASH + result));
        }
        return result;
    }

    public static void deleteFile(String fileName) {
        final File file = new File(SLASH + fileName);
        String name = file.getName();
        if (file.delete()) {
            log.info(FILE_IS_DELETED + name);
        } else {
            log.info(DELETE_FILE_FAILED + name);
        }
    }
}
