package com.epam.util;

import lombok.extern.log4j.Log4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    private static final String LOGGED_OUT_USER = "Logged out, user: ";
    private static final String LOGIN = "/login";

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                Authentication authentication) throws IOException {
        if (authentication != null && authentication.getDetails() != null) {
            try {
                log.info(LOGGED_OUT_USER + authentication.getName());
                httpServletRequest.getSession().invalidate();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        httpServletResponse.sendRedirect(LOGIN);
    }
}
