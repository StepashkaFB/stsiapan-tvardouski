package com.epam.repository;

import com.epam.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {
    Product getByNameOfProduct(String name);
    List<Product> getByCategory_Id(Integer id);
    boolean existsByNameOfProduct(String name);
}
