package com.epam.repository;

import com.epam.model.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasketRepository extends JpaRepository<Basket,Integer> {
    List<Basket> getByUsername(String name);
    Basket getByProduct_IdAndUsername(Integer id, String name);
    void deleteByProduct_IdAndUsername(Integer id, String name);
    boolean existsByUsernameAndProduct_Id(String name, Integer id);
}
