package com.epam.repository;

import com.epam.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {
    Category getByNameOfCategory(String name);
    void deleteByNameOfCategory(String name);
    boolean existsByNameOfCategory(String name);
}
