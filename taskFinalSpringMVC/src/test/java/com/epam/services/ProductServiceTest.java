package com.epam.services;

import com.epam.configuration.Config;
import com.epam.exceptions.ProductExistException;
import com.epam.model.Category;
import com.epam.model.Product;
import com.epam.repository.CategoryRepository;
import com.epam.repository.ProductRepository;
import com.epam.util.FileUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class ProductServiceTest {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    ProductService productService;
    @Autowired
    CategoryRepository categoryRepository;

    private static Category category = mock(Category.class);
    private static Product product = mock(Product.class);
    private static List<Product> products = new ArrayList<>();
    private static MultipartFile multipartFile = mock(MultipartFile.class);


    @BeforeAll
    static void setup() throws IOException {
        products.add(product);
        products.add(product);
        when(product.getNameOfProduct()).thenReturn("TestName");
        when(product.getId()).thenReturn(1);
        when(multipartFile.getOriginalFilename()).thenReturn("FileName");
        doThrow(new IOException()).when(multipartFile).transferTo(any(File.class));
    }

    @BeforeEach
    void init() {
        when(categoryRepository.findById(anyInt()))
                .thenReturn(Optional.ofNullable(category));
        when(productRepository.findById(anyInt()))
                .thenReturn(Optional.ofNullable(product));
    }

    @Test
    void getAll() {
        when(productRepository.findAll()).thenReturn(products);
        int size = products.size();
        assertArrayEquals(products.toArray(new Product[size]),
                productService.getAll().toArray(new Product[size]));
    }

    @PrepareForTest({FileUtil.class})
    @Test
    void save() {
        PowerMockito.mockStatic(FileUtil.class);
        assertThrows(IOException.class, () ->
                PowerMockito.when(FileUtil.addFile(multipartFile))
                        .thenReturn("Way"));
        when(productRepository.getByNameOfProduct(anyString()))
                .thenReturn(product)
                .thenReturn(null);
        assertThrows(ProductExistException.class, () ->
                productService.save(product, 1, multipartFile));
        verify(productRepository, never()).save(product);

        assertThrows(IOException.class, () ->
                productService.save(product, 1, multipartFile));
    }

    @Test
    void update() {
        assertThrows(IOException.class, () ->
                productService.update(product, 1, multipartFile));
    }


    @Test
    void getProductByIdOfCategory() {
        when(productRepository.getByCategory_Id(anyInt()))
                .thenReturn(products);
        int size = products.size();
        assertArrayEquals(products.toArray(new Product[size]),
                productService.getProductByIdOfCategory(1)
                        .toArray(new Product[size]));
    }

    @Test
    void deleteById() {
        productService.deleteById(1);
        verify(productRepository,times(1)).deleteById(1);
    }
}