package com.epam.services;

import com.epam.configuration.Config;
import com.epam.model.User;
import com.epam.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class UserDetailsServiceCustomTest {
    private static final String TEST_NAME = "TestName";
    private static final String PASSWORD = "qwerty";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserDetailsService userDetailsService;
    private static User user = mock(User.class);

    @BeforeAll
    static void setup() {
        when(user.getUsername()).thenReturn(TEST_NAME);
        when(user.getPassword()).thenReturn(PASSWORD);
        when(user.getRole()).thenReturn(ROLE_ADMIN);
    }

    @Test
    void loadUserByUsername() {
        when(userRepository.getUserByUsername(anyString()))
                .thenReturn(user)
                .thenReturn(null);
        UserDetails userDetails =
                userDetailsService.loadUserByUsername(TEST_NAME);
        assertEquals(TEST_NAME,userDetails.getUsername());
        assertEquals(PASSWORD,userDetails.getPassword());
        assertThrows(UsernameNotFoundException.class,()->
                userDetailsService.loadUserByUsername(TEST_NAME));
    }
}