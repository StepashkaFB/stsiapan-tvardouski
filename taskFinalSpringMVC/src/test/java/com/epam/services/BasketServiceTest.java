package com.epam.services;

import com.epam.configuration.Config;
import com.epam.model.Basket;
import com.epam.model.Product;
import com.epam.repository.BasketRepository;
import com.epam.repository.ProductRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class BasketServiceTest {

    private static Basket basket = mock(Basket.class);
    private static Product product = mock(Product.class);
    private static List<Basket> baskets = new ArrayList<>();

    @Autowired
    BasketRepository basketRepository;
    @Autowired
    BasketService basketService;
    @Autowired
    ProductRepository productRepository;

    @BeforeAll
    static void setup() {
        when(product.getNameOfProduct()).thenReturn("TestProduct");
        when(product.getAmountOfProduct()).thenReturn(10);
        when(basket.getProduct()).thenReturn(product);
        baskets.add(basket);
        baskets.add(basket);
    }
    @BeforeEach
    void init(){
        when(basketRepository.getByProduct_IdAndUsername(anyInt(), anyString()))
                .thenReturn(basket)
                .thenReturn(null);
    }

    @ParameterizedTest
    @CsvSource({"1,Test"})
    void save(Integer id, String str) {
        basketService.save(id, str);
        verify(basket, times(1)).increaseAmount();
        verify(basketRepository, never()).save(any(Basket.class));
        when(productRepository.findById(anyInt())).thenReturn(Optional.ofNullable(product));
        basketService.save(id, str);
        verify(basketRepository, times(1)).save(any(Basket.class));
    }

    @Test
    void delete() {
        basketService.delete(anyInt(),anyString());
        verifyDelete(1);
    }

    private void verifyDelete(int i) {
        verify(basketRepository, times(i))
                .deleteByProduct_IdAndUsername(anyInt(), anyString());
    }

    @Test
    void buyAndDeleteProduct() {
        basketService.buyAndDeleteProduct(anyInt(),anyString(),1);
        verifyDelete(2);
        assertThrows(IllegalArgumentException.class, () ->
                basketService.buyAndDeleteProduct(anyInt(),anyString(),11));
        verify(basket.getProduct(),times(1)).decreaseAmount(anyInt());
    }

    @Test
    void getByUserName() {
        String temp= "any";
        when(basketRepository.getByUsername(temp)).thenReturn(baskets);
        int size = baskets.size();
        assertArrayEquals(basketService.getByUserName(temp).toArray(new Basket[size]),
                basketRepository.getByUsername(temp).toArray(new Basket[size]));
    }
}