package com.epam.services;

import com.epam.configuration.Config;
import com.epam.exceptions.CategoryExistException;
import com.epam.model.Category;
import com.epam.repository.CategoryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;
    private List<Category> categories;
    private static final Category category = mock(Category.class);

    @BeforeEach
    void init() {
        categories = new ArrayList<>();
        categories.add(new Category(1, "TestOne"));
        categories.add(new Category(2, "TestSecond"));
    }

    @BeforeAll
    static void setup() {
        when(category.getNameOfCategory()).thenReturn("Test");
    }

    @Test
    void getAll() {
        when(categoryRepository.findAll()).thenReturn(categories);
        int size = categories.size();
        assertArrayEquals(categories.toArray(new Category[size]),
                categoryService.getAll().toArray(new Category[size]));
        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    void getCategoryById() {
        when(categoryRepository.findById(anyInt())).thenReturn(Optional.ofNullable(category));
        assertEquals(category, categoryService.getCategoryById(1));
        verify(categoryRepository, times(1)).findById(1);
    }

    @Test
    void save() {
        when(categoryRepository.getByNameOfCategory(any(String.class)))
                .thenReturn(null);
        when(categoryRepository.save(any(Category.class))).thenReturn(category);
        assertEquals(category, categoryRepository.save(category));
        categoryService.save(category);
        verify(categoryRepository, times(2)).save(category);
    }

    @Test
    void saveExpectedException() {
        when(categoryRepository.getByNameOfCategory(any(String.class)))
                .thenReturn(category);
        when(categoryRepository.save(any(Category.class))).thenReturn(category);
        assertThrows(CategoryExistException.class, () -> categoryService.save(category));
    }
}