package com.epam.services;

import com.epam.configuration.Config;
import com.epam.model.User;
import com.epam.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class UserServiceTest {
    private static final String TEST_NAME = "TestName";
    private static final String PASSWORD = "qwerty";
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    private static User user = mock(User.class);

    @BeforeAll
    static void setup() {
        when(user.getUsername()).thenReturn(TEST_NAME);
        when(user.getPassword()).thenReturn(PASSWORD);
    }

    @Test
    void getUserByName() {
        when(userRepository.getUserByUsername(TEST_NAME))
                .thenReturn(user);
        assertEquals(user,userService.getUserByName(user));
    }

    @Test
    void saveUser() {
        userService.saveUser(user);
        verify(userRepository,times(1)).save(user);
    }
}