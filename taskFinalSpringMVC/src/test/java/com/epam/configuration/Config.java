package com.epam.configuration;

import com.epam.repository.BasketRepository;
import com.epam.repository.CategoryRepository;
import com.epam.repository.ProductRepository;
import com.epam.repository.UserRepository;
import com.epam.services.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.Mockito.mock;

@Configuration
public class Config {

    @Bean("testCategoryRepository")
    public CategoryRepository categoryRepository() {
        return mock(CategoryRepository.class);
    }

    @Bean("testBasketRepository")
    public BasketRepository basketRepository() {
        return mock(BasketRepository.class);
    }

    @Bean("testProductRepository")
    public ProductRepository productRepository() {
        return mock(ProductRepository.class);
    }

    @Bean("testUserRepository")
    public UserRepository userRepository() {
        return mock(UserRepository.class);
    }

    @Bean("testCategoryService")
    public CategoryService categoryService() {
        return new CategoryService(categoryRepository());
    }

    @Bean("testProductService")
    public ProductService productService() {
        return new ProductService(productRepository(), categoryService());
    }

    @Bean("testBasketService")
    public BasketService basketService() {
        return new BasketService(basketRepository(), productService());
    }

    @Bean
    public BCryptPasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean("testUserService")
    public UserService userService() {
        return new UserService(userRepository(), getPasswordEncoder());
    }

    @Bean
    public UserDetailsService userDetailsService() {
        UserDetailsServiceCustom userDetailsService =
                new UserDetailsServiceCustom();
        userDetailsService.setUserRepository(userRepository());
        return userDetailsService;
    }
}
