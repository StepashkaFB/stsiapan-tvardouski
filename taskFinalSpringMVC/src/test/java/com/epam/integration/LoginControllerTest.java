package com.epam.integration;

import com.epam.config.*;
import com.epam.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@ComponentScan("com.epam")
@ContextConfiguration(classes = {
        WebAppInitializer.class,
        PersistenceConfig.class, SecurityInit.class,
        SpringSecurityConfig.class, WebConfig.class
})
@ExtendWith(SpringExtension.class)
class LoginControllerTest {
    private static final String VIEW_LOGIN = "login";
    private static final String ERROR = "error";
    private static final String VIEW_ACCESS_DENIED = "accessDenied";
    private static final String URL_USER_GET_PRODUCTS = "/user/getProducts";
    private static final String URL_POST_LOGIN = "/postLogin";

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mvc;
    @Autowired
    ProductService productService;

    static RequestPostProcessor testUser() {
        return user("tt").roles("ADMIN", "USER");
    }

    @BeforeEach
    void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .apply(springSecurity())
                .build();
    }

    @Test
    void accessDenied() throws Exception {
        mvc.perform(get("/accessDenied")
                .with(testUser()))
                .andDo(print())
                .andExpect(view().name(VIEW_ACCESS_DENIED));
    }

    @Test
    void loginError() throws Exception {
        mvc.perform(get("/loginFailed"))
                .andDo(print())
                .andExpect(model().attributeExists(ERROR))
                .andExpect(view().name(VIEW_LOGIN));
    }
    @Test
    void login() throws Exception {
        mvc.perform(get("/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(VIEW_LOGIN));
    }

    @Test
    void postLogin() throws Exception {
        mvc.perform(post(URL_POST_LOGIN)
                .with(testUser()))
                .andDo(print())
                .andExpect(redirectedUrl(URL_USER_GET_PRODUCTS));
    }

    @Test
    void doLogin() throws Exception {
        mvc.perform(formLogin("/doLogin")
                .user("tt")
                .password("test")).andDo(print())
                .andExpect(forwardedUrl(URL_POST_LOGIN));
    }
}
