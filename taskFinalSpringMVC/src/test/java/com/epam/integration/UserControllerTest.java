package com.epam.integration;

import com.epam.config.*;
import com.epam.model.Product;
import com.epam.repository.BasketRepository;
import com.epam.repository.ProductRepository;
import com.epam.services.BasketService;
import com.epam.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@ComponentScan("com.epam")
@ContextConfiguration(classes = {
        WebAppInitializer.class,
        PersistenceConfig.class, SecurityInit.class,
        SpringSecurityConfig.class, WebConfig.class
})
@ExtendWith(SpringExtension.class)
class UserControllerTest {
    private static final String NAME_OF_PRODUCT = "NameOfProduct";
    private static final String CURRENT_USER = "currentUser";
    private static final String ID = "id";
    private static final String REDIRECT_SHOW_BASKET = "showBasket";
    private static final String NAME = "tt";
    private static final String AMOUNT = "amount";
    private static final String URL_BASKET = "basket";
    private static final String ATT_BASKETS = "baskets";
    private static final String VALUE_ID = "1";
    private static final String VIEW_USER = "user";
    private static final String PRODUCTS = "products";
    private static final String CATEGORIES = "categories";
    private static final String TEST_DESCRIPTION = "TestDescription";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String ROLE_USER = "USER";
    @Autowired
    private ProductService productService;
    @Autowired
    private BasketService basketService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private BasketRepository basketRepository;
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mvc;
    private Product product;


    @BeforeEach
    void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .apply(springSecurity())
                .build();
        product = new Product();
        product.setNameOfProduct(NAME_OF_PRODUCT);
        product.setDescription(TEST_DESCRIPTION);
        product.setPrice(10);
        product.setAmountOfProduct(10);
    }

    @Test
    void getProducts() throws Exception {
        mvc.perform(get("/user/getProducts")
                .with(testUser()))
                .andDo(print())
                .andExpect(model().attributeExists(PRODUCTS))
                .andExpect(model().attributeExists(CATEGORIES))
                .andExpect(view().name(VIEW_USER));

    }

    static RequestPostProcessor testUser() {
        return user(NAME).roles(ROLE_ADMIN, ROLE_USER);
    }

    @Test
    void addToCard() throws Exception {
        mvc.perform(post("/user/addToBasket")
                .sessionAttr(CURRENT_USER, NAME)
                .param(ID, VALUE_ID)
                .with(testUser()));
    }

    @Test
    void showBasket() throws Exception {
        mvc.perform(get("/user/showBasket")
                .sessionAttr(CURRENT_USER, NAME)
                .with(testUser()))
                .andDo(print())
                .andExpect(model().attributeExists(ATT_BASKETS))
                .andExpect(view().name(URL_BASKET));
    }

    @Test
    @Transactional
    void buyProduct() throws Exception {
        productService.save(product, 1, null);
        Product temp = productRepository.getByNameOfProduct(NAME_OF_PRODUCT);
        final String name = NAME;
        final Integer id = temp.getId();
        basketService.save(id, name);
        mvc.perform(post("/user/buy")
                .with(testUser())
                .sessionAttr(CURRENT_USER, name)
                .param(ID, String.valueOf(id))
                .param(AMOUNT, String.valueOf(1)))
                .andDo(print())
                .andExpect(redirectedUrl(REDIRECT_SHOW_BASKET));
        productService.deleteById(id);
    }

    @Test
    @Transactional
    void deleteProduct() throws Exception {
        productService.save(product, 1, null);
        Product temp = productRepository.getByNameOfProduct(NAME_OF_PRODUCT);
        final String name = NAME;
        final Integer id = temp.getId();
        basketService.save(id, name);
        mvc.perform(post("/user/delete")
                .with(testUser())
                .sessionAttr(CURRENT_USER, name)
                .param(ID, String.valueOf(id)))
                .andDo(print())
                .andExpect(redirectedUrl(REDIRECT_SHOW_BASKET));
        assertFalse(basketRepository.existsByUsernameAndProduct_Id(name, id));
        productService.deleteById(id);
    }
}