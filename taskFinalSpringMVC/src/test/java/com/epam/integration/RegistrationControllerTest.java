package com.epam.integration;

import com.epam.config.*;
import com.epam.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;


import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@ComponentScan("com.epam")
@ContextConfiguration(classes = {
        WebAppInitializer.class,
        PersistenceConfig.class, SecurityInit.class,
        SpringSecurityConfig.class, WebConfig.class
})
@ExtendWith(SpringExtension.class)
class RegistrationControllerTest {
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_ROLE = "role";
    private static final String TEST = "test";
    private static final String ROLE = "ROLE_USER";
    private static final String LOGIN = "/login";
    public static final String URL_REGISTRATION = "registration";
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mvc;
    @Autowired
    UserRepository userRepository;

    @BeforeEach
    void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .build();
    }

    @Test
    @Transactional
    void registrationUser() throws Exception {
        final String testUser = "TestUser";
        mvc.perform(post("/sign_up")
                .param(PARAM_USERNAME, testUser)
                .param(PARAM_PASSWORD, TEST)
                .param(PARAM_ROLE, ROLE))
                .andDo(print())
                .andExpect(redirectedUrl(LOGIN));
        assertTrue(userRepository.existsByUsername(testUser));
        userRepository.deleteByUsername(testUser);
    }

    @Test
    void getRegistration() throws Exception {
        mvc.perform(get("/reg"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name(URL_REGISTRATION));
    }
}