package com.epam.integration;

import com.epam.config.*;
import com.epam.model.Product;
import com.epam.repository.CategoryRepository;
import com.epam.repository.ProductRepository;
import com.epam.services.CategoryService;
import com.epam.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@ComponentScan("com.epam")
@ContextConfiguration(classes = {
        WebAppInitializer.class,
        PersistenceConfig.class, SecurityInit.class,
        SpringSecurityConfig.class, WebConfig.class
})
@ExtendWith(SpringExtension.class)
class AdminControllerTest {
    private static final String TEST_CATEGORY = "testCategory";
    private static final String TEST = "test";
    private static final String NAME_OF_PRODUCT = "nameOfProduct";
    private static final String ID = "id";
    private static final String PRODUCTS = "products";
    private static final String CATEGORIES = "categories";
    private static final String PRODUCT = "product";
    private static final String CATEGORY = "category";
    private static final String VIEW_ADMIN = "admin";
    private static final String ONE_CATEGORY = "cat";
    private static final String VIEW_UPDATE = "update";
    private static final String DESCRIPTION = "description";
    private static final String AMOUNT_OF_PRODUCT = "amountOfProduct";
    private static final String PRICE = "price";
    private static final String ID_CATEGORY = "idCat";
    private static final String URL_GET_ALL = "getAll";
    private static final String NAME_OF_CATEGORY = "nameOfCategory";
    private static final String FILE_NAME = "file";
    private static final String ORIGINAL_NAME = "filename.txt";
    private static final String CONTENT_TYPE = "text/plain";
    private static final String TEXT_VALUE = "some xml";
    private static final String TEST_DESCRIPTION = "TestDescription";
    private static final String VALUE_AMOUNT_PRODUCT = "4";
    private static final String VALUE_PRICE = "5";
    private static final String VALUE_CATEGORY = "1";

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mvc;

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryRepository categoryRepository;
    private Product product;

    static RequestPostProcessor testUser() {
        return user("tt").roles("ADMIN", "USER");
    }

    @BeforeEach
    void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .apply(springSecurity())
                .build();
        product = new Product();
        product.setId(1000);
        product.setNameOfProduct(NAME_OF_PRODUCT);
        product.setDescription(TEST_DESCRIPTION);
        product.setPrice(10);
        product.setAmountOfProduct(10);
    }

    @Test
    void getProducts() throws Exception {
        mvc.perform(get("/admin/getAll")
                .with(testUser()))
                .andDo(print())
                .andExpect(model().attribute(PRODUCTS, productService.getAll()))
                .andExpect(model().attribute(CATEGORIES, categoryService.getAll()))
                .andExpect(model().attributeExists(PRODUCT))
                .andExpect(model().attributeExists(CATEGORY))
                .andExpect(view().name(VIEW_ADMIN));
    }

    @Test
    @Transactional
    void editPage() throws Exception {
        productService.save(product, 1, null);
        final Integer id = productRepository
                .getByNameOfProduct(NAME_OF_PRODUCT).getId();
        System.out.println("________________" + id);
        mvc.perform(get("/admin/updatePage")
                .with(testUser())
                .param(ID, String.valueOf(id)))
                .andDo(print())
                .andExpect(model().attributeExists(PRODUCT))
                .andExpect(model().attributeExists(ONE_CATEGORY))
                .andExpect(model().attributeExists(CATEGORIES))
                .andExpect(status().isOk())
                .andExpect(view().name(VIEW_UPDATE));
        productService.deleteById(id);
    }

    @Test
    void updateProduct() throws Exception {
        productService.save(product, 1, null);
        Product temp = productRepository.getByNameOfProduct(NAME_OF_PRODUCT);
        final String newDescription = "NewDescription";
        mvc.perform(post("/admin/updateProduct")
                .with(testUser())
                .param(ID, String.valueOf(temp.getId()))
                .param(NAME_OF_PRODUCT, product.getNameOfProduct())
                .param(DESCRIPTION, newDescription)
                .param(AMOUNT_OF_PRODUCT, String.valueOf(temp.getAmountOfProduct()))
                .param(PRICE, String.valueOf(temp.getPrice()))
                .param(ID_CATEGORY, String.valueOf(temp.getCategory().getId())))
                .andExpect(redirectedUrl(URL_GET_ALL));
        temp = productRepository.getByNameOfProduct(NAME_OF_PRODUCT);
        assertEquals(newDescription, temp.getDescription());
        productService.deleteById(temp.getId());
    }

    @Test
    void deleteProduct() throws Exception {
        productService.save(product, 1, null);
        mvc.perform(post("/admin/deleteProduct")
                .with(testUser())
                .param(ID, String.valueOf(productRepository
                        .getByNameOfProduct(NAME_OF_PRODUCT).getId())))
                .andDo(print())
                .andExpect(redirectedUrl(URL_GET_ALL));
    }

    @Test
    @Transactional
    void addProduct() throws Exception {
        MockMultipartFile firstFile
                = new MockMultipartFile(FILE_NAME, ORIGINAL_NAME,
                CONTENT_TYPE, TEXT_VALUE.getBytes());
        mvc.perform(MockMvcRequestBuilders.multipart("/admin/addProduct")
                .file(firstFile).param(NAME_OF_PRODUCT, TEST)
                .with(testUser())
                .param(DESCRIPTION, TEST)
                .param(AMOUNT_OF_PRODUCT, VALUE_AMOUNT_PRODUCT)
                .param(PRICE, VALUE_PRICE)
                .param(ID_CATEGORY, VALUE_CATEGORY))
                .andDo(print())
                .andExpect(redirectedUrl(URL_GET_ALL));
        assertTrue(productRepository.existsByNameOfProduct(TEST));
        productService.deleteById(productRepository.getByNameOfProduct(TEST).getId());
    }

    @Test
    @Transactional
    void addCategory() throws Exception {
        if (categoryRepository.existsByNameOfCategory(TEST_CATEGORY)) {
            categoryRepository.deleteByNameOfCategory(TEST_CATEGORY);
        }
        mvc.perform(post("/admin/addCategory")
                .with(testUser())
                .param(NAME_OF_CATEGORY, TEST_CATEGORY))
                .andExpect(redirectedUrl(URL_GET_ALL));
        categoryRepository.deleteByNameOfCategory(TEST_CATEGORY);
    }
}