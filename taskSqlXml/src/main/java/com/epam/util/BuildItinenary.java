package com.epam.util;

import com.datalex.ota.rqrs.air.extensions.types.fareinfo.*;

import java.util.ArrayList;
import java.util.List;

public class BuildItinenary {
    public static ItineraryType getItinerary() {
        ItineraryType itineraryType = new ItineraryType();
        OptionsType optionsType = new OptionsType();
        OptionType optionType = new OptionType();

        List<FlightType> flight = new ArrayList<>();
        FlightType flightTypeFirst = getFlightType();

        FlightSupplementalInfoType flightSupplementalInfoTypeF
                = new FlightSupplementalInfoType();
        flightSupplementalInfoTypeF.setFlyingTime("PT2H6K");

        BookingClassAvailabilityType bookingClassAvailabilityTypeF
                = getBookingClassAvailabilityType();

        TerminalInformationType terminalInformationType = new TerminalInformationType();
        terminalInformationType.setTerminal("2");

        InventorySystemType inventorySystemType = new InventorySystemType();
        inventorySystemType.setName("SSC");

        ExtensionType extensionTypeFirst = getExtensionType(flightSupplementalInfoTypeF,
                bookingClassAvailabilityTypeF, terminalInformationType, inventorySystemType);

        flightTypeFirst.setExtension(extensionTypeFirst);
        flight.add(flightTypeFirst);
        flight.add(flightTypeFirst);
        optionType.setFlight(flight);
        optionType.setExtension(getExtensionTypeInvSour());
        optionsType.setOption(optionType);

        AirType airType = getAirType();
        airType.setOptions(optionsType);
        airType.setExtension(getExtensionType());

        PricingInfoType pricingInfoType = new PricingInfoType();
        pricingInfoType.setSource("Private");

        TotalType totalType = new TotalType();
        BaseType baseType = getBaseType();
        TaxesType taxesType = new TaxesType();
        taxesType.setTax(getTaxTypes());

        FeesType feesType = new FeesType();
        FeeType feeType = getFeeType();
        feesType.setFee(feeType);

        TotalType totalType1 = getTotalType();
        ExtensionType extensionType = new ExtensionType();
        TaxInfosType taxInfosType = new TaxInfosType();
        taxInfosType.setTaxInfo(getTaxInfoTypes());
        extensionType.setTaxInfos(taxInfosType);

        FeeInfosType feeInfosType = new FeeInfosType();
        FeeInfoType feeInfoType = getFeeInfoType();
        feeInfosType.setFeeInfo(feeInfoType);
        extensionType.setFeeInfos(feeInfosType);

        addContent(totalType, baseType, taxesType, feesType, totalType1, extensionType);
        pricingInfoType.setTotal(totalType);

        PTCsType ptCsType = new PTCsType();
        PTCType ptcType = new PTCType();
        ptcType.setSource("Private");
        PassengerQuantityType passengerQuantityType = getPassengerQuantityType();
        ptcType.setPassengerQuantity(passengerQuantityType);

        BasisCodesType basisCodesType = new BasisCodesType();
        basisCodesType.setBasisCode("ILLIA");
        ptcType.setBasisCodes(basisCodesType);

        PassengerFareType passengerFareType = getPassengerFareType(baseType, taxesType,
                feesType, totalType1, extensionType);
        ptcType.setPassengerFare(passengerFareType);

        ExtensionType extensionTypePTC = new ExtensionType();
        PassengerType passengerType = getPassengerType();
        RemoteSystemPTCsType remoteSystemPTCsType = new RemoteSystemPTCsType();
        remoteSystemPTCsType.setPassengerCode("ADT");
        passengerType.setRemoteSystemPTCs(remoteSystemPTCsType);
        extensionTypePTC.setPassenger(passengerType);
        ptcType.setExtension(extensionTypePTC);
        ptCsType.setPTC(ptcType);
        pricingInfoType.setPTCs(ptCsType);

        FareInfosType fareInfosType = new FareInfosType();
        FareInfoType fareInfoType = getFareInfoType();
        ExtensionType extensionTypeFareInfo = getExtensionType(baseType, totalType1,
                taxInfosType, feeInfosType, passengerType);
        fareInfoType.setExtension(extensionTypeFareInfo);

        fareInfosType.setFareInfo(fareInfoType);
        pricingInfoType.setFareInfos(fareInfosType);

        ExtensionType extensionTypePrInfo = new ExtensionType();
        ProviderType providerType = new ProviderType();
        providerType.setProviderCode("EEEERERR");
        extensionTypePrInfo.setProvider(providerType);
        pricingInfoType.setExtension(extensionTypePrInfo);

        TicketingInfoType ticketingInfoType = new TicketingInfoType();
        ticketingInfoType.setTicketType("eTicket");

        itineraryType.setAir(airType);
        itineraryType.setPricingInfo(pricingInfoType);
        itineraryType.setTicketingInfo(ticketingInfoType);
        setValuesForItinerary(itineraryType);
        return itineraryType;
    }

    private static PassengerFareType getPassengerFareType(BaseType baseType, TaxesType taxesType,
                                                          FeesType feesType, TotalType totalType1,
                                                          ExtensionType extensionType) {
        PassengerFareType passengerFareType = new PassengerFareType();
        passengerFareType.setBase(baseType);
        passengerFareType.setTaxes(taxesType);
        passengerFareType.setFees(feesType);
        passengerFareType.setTotal(totalType1);
        passengerFareType.setExtension(extensionType);
        return passengerFareType;
    }

    private static ExtensionType getExtensionType(BaseType baseType, TotalType totalType1,
                                                  TaxInfosType taxInfosType, FeeInfosType feeInfosType,
                                                  PassengerType passengerType) {
        ExtensionType extensionTypeFareInfo = new ExtensionType();
        extensionTypeFareInfo.setBase(baseType);
        extensionTypeFareInfo.setTaxInfos(taxInfosType);
        extensionTypeFareInfo.setFeeInfos(feeInfosType);
        extensionTypeFareInfo.setTotal(totalType1);
        extensionTypeFareInfo.setSegment(getSegmentTypes());
        extensionTypeFareInfo.setPassenger(passengerType);
        extensionTypeFareInfo.setCarrier("LH");
        return extensionTypeFareInfo;
    }

    private static void addContent(TotalType totalType, BaseType baseType,
                                   TaxesType taxesType, FeesType feesType,
                                   TotalType totalType1, ExtensionType extensionType) {
        totalType.addContent(baseType);
        totalType.addContent(taxesType);
        totalType.addContent(feesType);
        totalType.addContent(totalType1);
        totalType.addContent(extensionType);
    }

    private static ExtensionType getExtensionType(FlightSupplementalInfoType flightSupplementalInfoTypeF,
                                                  BookingClassAvailabilityType bookingClassAvailabilityTypeF,
                                                  TerminalInformationType terminalInformationType,
                                                  InventorySystemType inventorySystemType) {
        ExtensionType extensionTypeFirst = new ExtensionType();
        extensionTypeFirst.setEticket("E");
        extensionTypeFirst.setFlightSupplementalInfo(flightSupplementalInfoTypeF);
        extensionTypeFirst.setBookingClassAvailability(bookingClassAvailabilityTypeF);
        extensionTypeFirst.setTerminalInformation(terminalInformationType);
        extensionTypeFirst.setInventorySystem(inventorySystemType);
        return extensionTypeFirst;
    }

    private static void setValuesForItinerary(ItineraryType itineraryType) {
        itineraryType.setSequenceNumber("2234");
        itineraryType.setRPH("200");
        itineraryType.setItineraryID("121");
    }

    private static List<SegmentType> getSegmentTypes() {
        List<SegmentType> segmentTypeList = new ArrayList<>();
        SegmentType segmentType0 = new SegmentType();
        segmentType0.setFlightReference("0");
        SegmentType segmentType1 = new SegmentType();
        segmentType1.setFlightReference("1");
        segmentTypeList.add(segmentType0);
        segmentTypeList.add(segmentType1);
        return segmentTypeList;
    }

    private static FareInfoType getFareInfoType() {
        FareInfoType fareInfoType = new FareInfoType();
        fareInfoType.setDepartureDate("2018-12-29T09:00:00.000");
        fareInfoType.setFareReference("ASDFGHJ");
        return fareInfoType;
    }

    private static PassengerType getPassengerType() {
        PassengerType passengerType = new PassengerType();
        passengerType.setPassengerCode("ADT");
        passengerType.setPassengerReference("0");
        passengerType.setPassengerAge("35");
        return passengerType;
    }

    private static PassengerQuantityType getPassengerQuantityType() {
        PassengerQuantityType passengerQuantityType = new PassengerQuantityType();
        passengerQuantityType.setCode("ADT");
        passengerQuantityType.setQuantity("1");
        return passengerQuantityType;
    }

    private static FeeInfoType getFeeInfoType() {
        FeeInfoType feeInfoType = new FeeInfoType();
        feeInfoType.setTaxCode("EE");
        feeInfoType.setCurrencyCode("EUR");
        return feeInfoType;
    }

    private static List<TaxInfoType> getTaxInfoTypes() {
        List<TaxInfoType> taxInfoTypeList = new ArrayList<>();
        TaxInfoType taxInfoType = new TaxInfoType();
        taxInfoType.setTaxCode("AA");
        taxInfoType.setBaseCurrency("EUR");
        for (int i = 0; i < 4; i++) {
            taxInfoTypeList.add(taxInfoType);
        }
        return taxInfoTypeList;
    }

    private static TotalType getTotalType() {
        TotalType totalType1 = new TotalType();
        totalType1.setAmount("25.6");
        totalType1.setCurrencyCode("EUR");
        return totalType1;
    }

    private static FeeType getFeeType() {
        FeeType feeType = new FeeType();
        feeType.setTaxCode("EE");
        feeType.setAmount("10.00");
        feeType.setCurrencyCode("EUR");
        return feeType;
    }

    private static BaseType getBaseType() {
        BaseType baseType = new BaseType();
        baseType.setAmount("91.00");
        baseType.setCurrencyCode("EUR");
        return baseType;
    }

    private static List<TaxType> getTaxTypes() {
        List<TaxType> taxTypeList = new ArrayList<>();
        TaxType taxType1 = new TaxType();
        taxType1.setAmount("7.38");
        taxType1.setCurrencyCode("EUR");
        taxType1.setTaxCode("AA");
        for (int i = 0; i < 4; i++) {
            taxTypeList.add(taxType1);
        }
        return taxTypeList;
    }

    private static AirType getAirType() {
        AirType airType = new AirType();
        airType.setDirection("SuicideWay");
        airType.setTotalTravelTime("PT2H55M");
        return airType;
    }

    private static ExtensionType getExtensionTypeInvSour() {
        ExtensionType extensionTypeInvSour = new ExtensionType();
        InventorySystemType inventorySystemTypeF = new InventorySystemType();
        inventorySystemTypeF.setName("ABC");
        extensionTypeInvSour.setInventorySystem(inventorySystemTypeF);
        extensionTypeInvSour.setSource("GDS");
        return extensionTypeInvSour;
    }

    private static ExtensionType getExtensionType() {
        ExtensionType extensionTypeAtt = new ExtensionType();
        AttributesType attributesTypeTrip = new AttributesType();
        attributesTypeTrip.setTripType("OW");
        extensionTypeAtt.setAttributes(attributesTypeTrip);
        return extensionTypeAtt;
    }

    private static BookingClassAvailabilityType getBookingClassAvailabilityType() {
        BookingClassAvailabilityType bookingClassAvailabilityTypeF
                = new BookingClassAvailabilityType();
        bookingClassAvailabilityTypeF.setBookCode("P");
        bookingClassAvailabilityTypeF.setBookDesig("5");
        bookingClassAvailabilityTypeF.setCabin("Economy");
        return bookingClassAvailabilityTypeF;
    }

    private static FlightType getFlightType() {
        FlightType flightTypeFirst = new FlightType();
        flightTypeFirst.setArrivalDateTime("2020-11-29T10:05:00.000");
        flightTypeFirst.setBookCode("K");
        flightTypeFirst.setDepartureDateTime("2020-10-29T10:05:00.000");
        flightTypeFirst.setFlightNumber("1488");
        flightTypeFirst.setNumberInParty("3");
        flightTypeFirst.setRPH("0");
        flightTypeFirst.setStopQuantity("0");
        return flightTypeFirst;
    }
}
