
import com.datalex.ota.rqrs.air.extensions.types.fareinfo.ItineraryType;
import com.datalex.ota.rqrs.air.extensions.types.fareinfo.ObjectFactory;
import com.epam.util.BuildItinenary;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Runner {

    private static final String PATH = Runner.class.getResource("").getPath();
    private static final String XML_NAME = "main.xml";
    private static final String LOCAL_PART = "ns2:Itinerary";

    public static void main(String[] args) {
        try {
            System.out.println(PATH);
            marshal();
            unmarshall()
                    .getValue()
                    .getPricingInfo()
                    .getPTCs()
                    .getPTC()
                    .getPassengerFare()
                    .getTaxes()
                    .getTax()
                    .forEach(System.out::println);
        } catch (JAXBException | FileNotFoundException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void marshal() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(ItineraryType.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        JAXBElement<ItineraryType> jaxbElement =
                new JAXBElement<>(new QName(LOCAL_PART), ItineraryType.class, BuildItinenary.getItinerary());
        mar.marshal(jaxbElement, new File(PATH + XML_NAME));
    }

    private static JAXBElement<ItineraryType> unmarshall() throws JAXBException, FileNotFoundException, ClassNotFoundException {
        JAXBContext context = JAXBContext.newInstance(
                ClassLoader.getSystemClassLoader().loadClass(ObjectFactory.class.getName()));
        Unmarshaller unmarshaller = context.createUnmarshaller();
        return (JAXBElement<ItineraryType>) unmarshaller
                .unmarshal(new FileReader(PATH + XML_NAME));
    }
}


