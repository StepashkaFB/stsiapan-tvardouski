package com.epam.tests;

import com.epam.beans.Person;
import com.epam.beans.ValidationSystem;
import com.epam.beans.Validator;
import com.epam.exceptions.ValidationFailedException;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ValidationSystemTest {
    private ValidationSystem validationSystem = new ValidationSystem();

    @Test
    public void testValidateInt () throws ValidationFailedException {
        assertTrue(validationSystem.validation(1));
        assertTrue(validationSystem.validation(5));
        assertTrue(validationSystem.validation(10));
    }

    @Test (expected = ValidationFailedException.class)
    public void testUnknownValue() throws ValidationFailedException{
        validationSystem.validation(1.1);
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateIntFails() throws ValidationFailedException {
        validationSystem.validation(11);
    }

    @Test
    public void testValidateString() throws ValidationFailedException{
        assertTrue(validationSystem.validation("Hello"));
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidateStringFails() throws ValidationFailedException{
        validationSystem.validation("hello");
    }

    @Test
    public void testValidatePerson() throws ValidationFailedException{
        assertTrue(validationSystem.validation(new Person(17,161)));
    }

    @Test (expected = ValidationFailedException.class)
    public void testValidatePersonFails() throws ValidationFailedException{
        validationSystem.validation(new Person(15,150));
    }

    @Test
    public void testRegistration()throws ValidationFailedException{
        validationSystem.registration(new Validator<Character>() {
            @Override
            public boolean validate(Character value) throws ValidationFailedException {
                if(value == 'a'){
                    throw new ValidationFailedException(value);
                }
                return true;
            }

            @Override
            public String getName() {
                return "Character";
            }
        });
        assertTrue(validationSystem.validation('t'));
    }
}