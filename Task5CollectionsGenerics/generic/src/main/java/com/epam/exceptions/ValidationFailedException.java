package com.epam.exceptions;

public class ValidationFailedException extends Exception {
    private static final String INCORRECT_DATA = "Incorrect data: ";

    public ValidationFailedException(Object value) {
        super(INCORRECT_DATA + value);
    }
}
