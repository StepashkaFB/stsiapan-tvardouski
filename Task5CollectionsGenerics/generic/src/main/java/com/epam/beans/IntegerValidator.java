package com.epam.beans;

import com.epam.exceptions.ValidationFailedException;

public class IntegerValidator implements  Validator<Integer> {
    @Override
    public boolean validate(Integer value) throws ValidationFailedException {
        if(!(value >=1 && value <=10 || value >=15 && value <=30)){
            throw new ValidationFailedException(value);
        }
        return true;
    }

    @Override
    public String getName() {
        return "Integer";
    }
}
