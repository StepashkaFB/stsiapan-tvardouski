package com.epam.beans;

import com.epam.exceptions.ValidationFailedException;

public class StringValidator implements Validator<String> {

    public static final String REGEX = "^[A-Z][a-z]{1,14}$";

    @Override
    public boolean validate(String value) throws ValidationFailedException {
        if(!value.matches(REGEX)){
            throw new ValidationFailedException(value);
        }
        return true;
    }
    @Override
    public String getName() {
        return "String";
    }
}
