package com.epam.beans;

import com.epam.exceptions.ValidationFailedException;

public interface Validator<T> {
    boolean validate(T value) throws ValidationFailedException;
    String getName();
}
