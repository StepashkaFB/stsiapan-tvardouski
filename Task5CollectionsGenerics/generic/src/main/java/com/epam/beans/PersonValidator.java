package com.epam.beans;

import com.epam.exceptions.ValidationFailedException;

public class PersonValidator implements Validator<Person> {
    @Override
    public boolean validate(Person value) throws ValidationFailedException {
        if(!(value.getAge() > 16 && value.getHeight() > 160)){
            throw new ValidationFailedException(value);
        }
        return true;
    }
    @Override
    public String getName() {
        return "Person";
    }
}
