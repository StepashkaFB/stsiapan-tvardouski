package com.epam.beans;

import com.epam.exceptions.ValidationFailedException;

import java.util.HashMap;
import java.util.Map;

public class ValidationSystem {
    private Map<String, Validator> validators = new HashMap<>();

    public ValidationSystem(){
        validators.put("String", new StringValidator());
        validators.put("Integer", new IntegerValidator());
        validators.put("Person", new PersonValidator());
    }

    public <T> boolean validation(T t) throws ValidationFailedException {
        String simpleName = t.getClass().getSimpleName();
        if(!validators.keySet().contains(simpleName)){
            throw new ValidationFailedException(t);
        }
        return validators.get(simpleName).validate(t);
    }

    public void registration(Validator<?> validator){
        validators.put(validator.getName(),validator);
    }
}
