package com.epam.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SetOperations<T> {
    public Set<T> composition(Set<T> first, Set<T> second){
        return first.stream()
                .filter(second::contains)
                .collect(Collectors.toSet());
    }
    public Set<T> union(Set<T> first, Set<T> second){
        return Stream.concat(first.stream(),second.stream())
                .collect(Collectors.toSet());
    }
    public Set<T> difference(Set<T> first, Set<T> second){
        List<T> list = new ArrayList<>(first);
        list.removeIf(second::contains);
        return new HashSet<>(list);
    }
    public Set<T> symmetricDifference(Set<T> first, Set<T> second){
        return difference(union(first,second),composition(first,second));
    }
}

