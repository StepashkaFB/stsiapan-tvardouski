package com.epam;

import com.epam.util.SetOperations;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

public class SetOperationsTest {
    private SetOperations<Integer>  setOperations = new SetOperations<>();
    private Set<Integer> setIntF = new HashSet<>(Arrays.asList(1,2,3,4));
    private Set<Integer> setIntS = new HashSet<>(Arrays.asList(3,4,5,6));
    private Set<Integer> testSet = new HashSet<>();

    @Test
    public void composition() {
        testSet = setOperations.composition(setIntF,setIntS);
        assertNotNull(testSet);
        assertEquals(testSet.size(),2);
        assertTrue(testSet.containsAll(Arrays.asList(3,4)));
    }

    @Test
    public void union() {
        testSet = setOperations.union(setIntF,setIntS);
        assertNotNull(testSet);
        assertEquals(testSet.size(),6);
        assertTrue(testSet.containsAll(Arrays.asList(1,2,3,4,5,6)));
    }

    @Test
    public void difference() {
        testSet = setOperations.difference(setIntF,setIntS);
        assertNotNull(testSet);
        assertEquals(testSet.size(),2);
        assertTrue(testSet.containsAll(Arrays.asList(1,2)));
    }

    @Test
    public void symmetricDifference() {
        testSet = setOperations.symmetricDifference(setIntF,setIntS);
        assertNotNull(testSet);
        assertEquals(testSet.size(),4);
        assertTrue(testSet.containsAll(Arrays.asList(1,2,5,6)));
    }
}