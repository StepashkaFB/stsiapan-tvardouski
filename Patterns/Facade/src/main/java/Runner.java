import com.epamlab.beans.OrderProduct;
import com.epamlab.beans.Shop;
import com.epamlab.beans.Stock;

public class Runner {
    public static void main(String[] args) {
        OrderProduct orderProduct = new OrderProduct(new Shop(),new Stock());
        orderProduct.order("car");
    }
}
