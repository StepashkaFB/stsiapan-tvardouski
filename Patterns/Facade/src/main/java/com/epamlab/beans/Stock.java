package com.epamlab.beans;

public class Stock {
    private String nameProduct;

    public void goToStock(String nameProduct){
        this.nameProduct = nameProduct;
        System.out.println("Search for products in stock: " + nameProduct);
    }
    public String packaging(){
        System.out.println(nameProduct + " - are packed");
        return "Packed " + nameProduct;
    }
}
