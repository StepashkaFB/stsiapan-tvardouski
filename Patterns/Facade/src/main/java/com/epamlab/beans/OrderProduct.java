package com.epamlab.beans;

public class OrderProduct {
    private Shop shop;
    private Stock stock;

    public OrderProduct(Shop shop, Stock stock) {
        this.shop = shop;
        this.stock = stock;
    }

    public void order(String name){
        stock.goToStock(name);
        shop.registration(stock.packaging());
        shop.deliveryProduct();
    }
}
