package com.epamlab.tests;
import com.epamlab.beans.Canella;
import com.epamlab.beans.Drink;
import com.epamlab.beans.Sugar;
import com.epamlab.beans.Tea;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDrink {
    private Drink tea = new Tea();

    @Test
    public void testTea(){
        assertEquals("Tea",tea.getInfo());
        assertEquals(200,tea.getPrice());
    }

    @Test
    public void testSugar(){
        tea = new Sugar(tea,2);
        assertEquals(250,tea.getPrice());
    }

    @Test
    public void testCanella(){
        tea = new Canella(tea,2);
        assertEquals(270,tea.getPrice());
    }

    @Test
    public void testCombine(){
        tea = new Sugar(new Canella(tea,2),2);
        assertEquals(320,tea.getPrice());
    }
}
