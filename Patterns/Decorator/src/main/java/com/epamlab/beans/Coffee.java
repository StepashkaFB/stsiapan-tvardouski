package com.epamlab.beans;

public class Coffee extends Drink {
    public Coffee(){
        name = "Coffee";
    }
    @Override
    public String getInfo() {
        return name;
    }

    @Override
    public int getPrice() {
        return 250;
    }
}
