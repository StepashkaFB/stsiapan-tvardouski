package com.epamlab.beans;

public class Tea extends Drink {
    public Tea(){
        name = "Tea";
    }
    @Override
    public String getInfo() {
        return name;
    }

    @Override
    public int getPrice() {
        return 200;
    }
}
