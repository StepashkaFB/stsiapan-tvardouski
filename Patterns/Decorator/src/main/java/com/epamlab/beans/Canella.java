package com.epamlab.beans;

import com.epamlab.exceptions.DataIncorrectException;

public class Canella  extends Decorator {
    private Drink drink;
    private int portion;

    public Canella(Drink drink, int portion){
        this.drink = drink;
        setPortion(portion);
    }

    @Override
    public int getPortion() {
        return portion;
    }

    public void setPortion(int portion){
        if(portion < 1){
            throw new DataIncorrectException(portion);
        }
        this.portion = portion;
    }
    @Override
    public String getInfo() {
        return drink.getInfo() + " + Cannela";
    }

    @Override
    public int getPrice() {
        return drink.getPrice() + (35*portion);
    }
}
