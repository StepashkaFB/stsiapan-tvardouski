package com.epamlab.beans;

public abstract class Decorator extends Drink {
    public abstract int getPortion();
}
