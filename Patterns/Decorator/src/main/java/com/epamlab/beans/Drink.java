package com.epamlab.beans;

public abstract class Drink {
    String name = "";
    public abstract String getInfo();
    public abstract int getPrice();
}
