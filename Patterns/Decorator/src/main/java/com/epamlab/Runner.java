package com.epamlab;

import com.epamlab.beans.Tea;
import com.epamlab.beans.Drink;
import com.epamlab.beans.Sugar;
import com.epamlab.beans.Canella;
import com.epamlab.beans.Coffee;
import com.epamlab.exceptions.DataIncorrectException;

public class Runner {

    public static final String PRICE = " Price = ";

    public static void main(String[] args) {
        try {
            Drink tea = new Tea();

            tea = new Sugar(tea, 2);
            tea = new Canella(tea, 3);
            System.out.println(tea.getInfo() + PRICE + tea.getPrice());

            Drink coffee = new Sugar(new Canella(new Coffee(), 2), 2);
            System.out.println(coffee.getInfo() + PRICE + coffee.getPrice());
        }catch (DataIncorrectException e){
            System.out.println(e.getMessage());
        }
    }
}
