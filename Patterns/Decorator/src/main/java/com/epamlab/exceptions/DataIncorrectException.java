package com.epamlab.exceptions;

public class DataIncorrectException extends RuntimeException {

    public static final String INCORRECT_DATA = "Incorrect data: ";

    public DataIncorrectException(int value) {
        super(INCORRECT_DATA + value);
    }

}