package com.epamlab.tests;

import com.epamlab.beans.Subject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.epamlab.exceptions.DataIncorrectException;
import org.junit.Test;

public class TestSubject {
    private Subject subject = new Subject("subject",4);

    @Test
    public void preview(){
        subject.preview();
        assertEquals(subject.getName(),subject.getUndo().getSubjectName());
        assertEquals(subject.getAmount(),subject.getUndo().getSubjectAmount());
    }

    @Test
    public void undoChanges(){
        try {
            subject.preview();
            subject.setName("new Subject");
            subject.setAmount(5);
            assertNotEquals(subject.getAmount(),subject.getUndo().getSubjectAmount());
            subject.setAmount(-3);
        }catch (DataIncorrectException e){
            subject.undoChanges();
            assertEquals("subject",subject.getName());
            assertEquals(4,subject.getAmount());
        }
    }
}
