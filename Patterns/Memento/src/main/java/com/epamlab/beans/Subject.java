package com.epamlab.beans;

import com.epamlab.exceptions.DataIncorrectException;

public class Subject {
    private String name;
    private int amount;
    private Memento undo = new Memento();

    public Subject(String name, int amount){
        this.name = name;
        setAmount(amount);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        if(amount <=0){
            throw new DataIncorrectException(amount);
        }
        this.amount = amount;
    }

    public Memento getUndo(){
        return undo;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "name='" + name + '\'' +
                ", amount=" + amount +
                ", undo=" + undo.subjectName + ";" + undo.subjectAmount +
                '}';
    }

    public class Memento {
        private String subjectName;
        private int subjectAmount;

        public Memento() {
            subjectName = name;
            subjectAmount = amount;
        }

        public String getSubjectName() {
            return subjectName;
        }

        public int getSubjectAmount() {
            return subjectAmount;
        }
    }

    public void preview() {
        undo = new Memento();
    }

    public void undoChanges() {
        name = undo.getSubjectName();
        amount = undo.getSubjectAmount();
    }

}

