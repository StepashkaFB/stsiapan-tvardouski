package com.epamlab;

import com.epamlab.beans.CentralSystem;
import com.epamlab.beans.ChangedSystemOut;
import com.epamlab.beans.SystemAdapter;
import com.epamlab.beans.SystemOut;
import com.epamlab.beans.OutAPI;

public class Runner {
    public static void main(String[] args) {
        OutAPI system = new SystemOut();
        CentralSystem centralSystem = new CentralSystem(system);
        centralSystem.work();

        //check adapter
        SystemAdapter systemAdapter = new SystemAdapter(new ChangedSystemOut());
        centralSystem.setApi(systemAdapter);
        centralSystem.work();
    }
}
