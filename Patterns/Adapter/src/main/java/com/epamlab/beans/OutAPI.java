package com.epamlab.beans;

public interface OutAPI {
    void getPrice();
    void validate();
    void bookTicket();
}
