package com.epamlab.beans;

public class CentralSystem {
    private OutAPI api;

    public CentralSystem(OutAPI api){
        this.api = api;
    }

    public void setApi(OutAPI api) {
        this.api = api;
    }

    public void work(){
        api.validate();
        api.getPrice();
        api.bookTicket();
    }
}
