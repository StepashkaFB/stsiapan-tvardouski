package com.epamlab.beans;

public class SystemAdapter implements OutAPI {
    ChangedOutAPI changedOutAPI;

    public SystemAdapter(ChangedOutAPI changedOutAPI){
        this.changedOutAPI = changedOutAPI;
    }

    @Override
    public void getPrice() {
        changedOutAPI.calculate();
    }

    @Override
    public void validate() {
        changedOutAPI.validateTicket();
    }

    @Override
    public void bookTicket() {
        changedOutAPI.book();
    }
}
