package com.epamlab.beans;

public interface ChangedOutAPI {
    void calculate();
    void validateTicket();
    void book();
}
