package com.epamlab.beans;

public class SystemOut implements OutAPI {
    @Override
    public void getPrice() {
        System.out.println("Getting a price: " + 100);
    }

    @Override
    public void validate() {
        System.out.println("Validation successful");
    }

    @Override
    public void bookTicket() {
        System.out.println("Get your book ticket");
    }
}
