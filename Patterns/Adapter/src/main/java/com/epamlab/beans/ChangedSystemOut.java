package com.epamlab.beans;

public class ChangedSystemOut implements ChangedOutAPI {
    @Override
    public void calculate() {
        System.out.println("Calculating the cost, cost = " + 25);
    }

    @Override
    public void validateTicket() {
        System.out.println("Validation a ticket... Successful!");
    }

    @Override
    public void book() {
        System.out.println("Get a book");
    }
}
