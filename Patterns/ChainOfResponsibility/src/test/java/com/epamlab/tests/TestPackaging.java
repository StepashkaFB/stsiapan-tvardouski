package com.epamlab.tests;
import com.epamlab.beans.Packaging;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestPackaging {
    private Packaging packaging = new Packaging(1);

    @Test
    public void check(){
        assertTrue(packaging.check(""));
        assertFalse(packaging.check(""));
    }
}
