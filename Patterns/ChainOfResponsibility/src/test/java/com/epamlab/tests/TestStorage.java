package com.epamlab.tests;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.epamlab.beans.Storage;
import org.junit.Test;

import java.time.LocalTime;

public class TestStorage {
    private Storage storage = new Storage(LocalTime.of(6,0), LocalTime.of(6,30));

    @Test
    public void check(){
        assertFalse(storage.check(""));
        storage.setStopWork(LocalTime.of(23,59));
        assertTrue(storage.check("airplane"));
    }
}
