package com.epamlab.beans;

import com.epamlab.exceptions.DataIncorrectException;

public class Packaging extends BasicCheck {

    private int amountOfPack;
    public Packaging(int amountOfPack){
        this.amountOfPack = amountOfPack;
    }
    public void setAmountOfPack(int amountOfPack){
        if(amountOfPack < 0){
            throw new DataIncorrectException(amountOfPack);
        }
    }
    @Override
    public boolean check(String nameOfProduct) {
        if(amountOfPack <=0){
            System.out.println("Not enough pack");
            return false;
        }
        amountOfPack--;
        return checkNext(nameOfProduct);
    }
}
