package com.epamlab.beans;

public class Transporter extends BasicCheck {
    private boolean work;

    public Transporter(){
        work = true;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    @Override
    public boolean check(String nameOfProduct) {
        if(!work){
            System.out.println("Transporter is not working");
            return false;
        }
        return checkNext(nameOfProduct);
    }
}
