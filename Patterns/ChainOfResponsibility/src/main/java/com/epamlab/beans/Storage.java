package com.epamlab.beans;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class Storage extends BasicCheck {
    private LocalTime startWork;
    private LocalTime stopWork;
    private List<Product> products = new ArrayList<>();

    public Storage(LocalTime startWork, LocalTime stopWork){
        this.startWork = startWork;
        this.stopWork = stopWork;
        basicInitialization();
    }
    public void basicInitialization(){
        products.add(new Product("machine"));
        products.add(new Product("airplane"));
    }

    public LocalTime getStartWork() {
        return startWork;
    }

    public void setStartWork(LocalTime startWork) {
        this.startWork = startWork;
    }

    public LocalTime getStopWork() {
        return stopWork;
    }

    public void setStopWork(LocalTime stopWork) {
        this.stopWork = stopWork;
    }

    @Override
    public boolean check(String nameOfProduct) {
        LocalTime now = LocalTime.now();
        if(now.isAfter(stopWork) || now.isBefore(startWork)){
            System.out.println("Storage is not working yet");
            return false;
        }
        if(!products.contains(new Product(nameOfProduct))){
            System.out.println("This item is out of stock.");
            return false;
        }
        return checkNext(nameOfProduct);
    }
}
