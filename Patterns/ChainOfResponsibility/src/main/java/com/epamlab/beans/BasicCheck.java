package com.epamlab.beans;

public  abstract class BasicCheck {
    private BasicCheck next;

    public BasicCheck link(BasicCheck next){
        this.next = next;
        return next;
    }

    public abstract boolean check(String nameOfProduct);

    protected boolean checkNext(String nameOfProduct) {
        if (next == null) {
            return true;
        }
        return next.check(nameOfProduct);
    }
}
