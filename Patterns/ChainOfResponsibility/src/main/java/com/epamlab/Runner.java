package com.epamlab;

import com.epamlab.beans.BasicCheck;
import com.epamlab.beans.Packaging;
import com.epamlab.beans.Stock;
import com.epamlab.beans.Storage;
import com.epamlab.beans.Transporter;
import com.epamlab.beans.OrderProduct;
import com.epamlab.beans.Shop;
import java.time.LocalTime;

public class Runner {

    public static final String NAME_OF_PRODUCT = "airplane";
    public static final LocalTime START_WORK = LocalTime.of(8, 0);
    public static final LocalTime STOP_WORK = LocalTime.of(21, 0);
    public static final int AMOUNT_OF_PACK = 4;

    public static void main(String[] args) {
        //create a chain
        BasicCheck basicCheck = new Storage(START_WORK, STOP_WORK);
        Packaging packaging = new Packaging(AMOUNT_OF_PACK);
        Transporter transporter = new Transporter();
        basicCheck.link(packaging).link(transporter);

        OrderProduct orderProduct = new OrderProduct(new Shop(),new Stock());

        if(basicCheck.check(NAME_OF_PRODUCT)){
            orderProduct.order(NAME_OF_PRODUCT);
        }
    }
}
