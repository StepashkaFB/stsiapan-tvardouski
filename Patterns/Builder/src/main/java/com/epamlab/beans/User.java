package com.epamlab.beans;

public class User {
    private int userId;
    private String name;
    private String mail;
    private String city;
    private String language;
    private boolean administrator;
    private boolean moderator;
    private boolean vipUser;

    private User(){

    }

    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getCity() {
        return city;
    }

    public String getLanguage() {
        return language;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public boolean isModerator() {
        return moderator;
    }

    public boolean isVipUser() {
        return vipUser;
    }

    public static Builder newBuilder(int id, String str) {
        return new User().new Builder(id,str);
    }

    public class Builder {

        private Builder(int userId, String name) {
            User.this.userId = userId;
            User.this.name = name;
        }

        public Builder setMail(String mail){
            User.this.mail = mail;
            return this;
        }

        public Builder setCity(String city){
            User.this.city = city;
            return this;
        }

        public Builder setLanguage(String language){
            User.this.language = language;
            return this;
        }

        public Builder setAdministrator(boolean administrator){
            User.this.administrator = administrator;
            return this;
        }

        public Builder setModerator(boolean moderator){
            User.this.moderator = moderator;
            return this;
        }

        public Builder setVipUser(boolean vipUser){
            User.this.vipUser = vipUser;
            return this;
        }
        public User build() {
            return User.this;
        }
    }
}
