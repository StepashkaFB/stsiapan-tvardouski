package com.epamlab.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.epamlab.beans.User;
import org.junit.Test;

public class UserTest
{

    @Test
    public void checkUser()
    {
        User user = User.newBuilder(1,"Test")
                .setAdministrator(true)
                .setLanguage("RU")
                .setCity("Gomel").build();

        assertTrue("This user should be a admin", user.isAdministrator());
        assertFalse("This user is not a moderator", user.isModerator());
    }
}
