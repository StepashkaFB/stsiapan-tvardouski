package com.epamlab;

import com.epamlab.factories.GomelFactory;
import com.epamlab.factories.MinskFactory;
import com.epamlab.beans.PlasticWindow;
import com.epamlab.beans.SpecialWindow;
import com.epamlab.beans.WindowFactory;
import com.epamlab.beans.WoodenWindow;

public class DemoFactory {
    private PlasticWindow plasticWindow;
    private WoodenWindow woodenWindow;
    private SpecialWindow specialWindow;
    private WindowFactory windowFactory;

    public DemoFactory(String factory){
        getFactory(factory);
        woodenWindow = windowFactory.createWoodenWindow();
        specialWindow = windowFactory.createSpecialWindow();
        plasticWindow = windowFactory.createPlasticWindow();
    }
    public void getFactory(String factory){
        if("gomel".equalsIgnoreCase(factory)){
            windowFactory = new GomelFactory();
            System.out.println(windowFactory.getName());
        }else if("minsk".equalsIgnoreCase(factory)){
            windowFactory = new MinskFactory();
            System.out.println(windowFactory.getName());
        }
    }
    public void show(){
        System.out.println(woodenWindow.getInfoWindow());
        System.out.println(specialWindow.getInfoWindow());
        System.out.println(plasticWindow.getInfoWindow());
    }

    public static void main(String[] args) {
        DemoFactory demoFactory = new DemoFactory("gomel");
        demoFactory.show();
    }
}
