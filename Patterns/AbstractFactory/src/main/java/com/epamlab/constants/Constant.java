package com.epamlab.constants;

public class Constant {
    public static final String NAME_OF_WINDOW = "Name of window: ";
    public static final String MATERIAL = "Material :";
    public static final String COST = "Cost: ";
    public static final String PLASTIC_WINDOW = "Plastic window";
    public static final String WOODEN_WINDOW = "Wooden window";
    public static final String SPECIAL_WINDOW = "Special window";
}
