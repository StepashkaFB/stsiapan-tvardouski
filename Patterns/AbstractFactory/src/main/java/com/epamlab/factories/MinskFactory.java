package com.epamlab.factories;

import com.epamlab.beans.PlasticWindowMinsk;
import com.epamlab.beans.SpecialWindowMinsk;
import com.epamlab.beans.WoodenWindowMinsk;
import com.epamlab.beans.PlasticWindow;
import com.epamlab.beans.SpecialWindow;
import com.epamlab.beans.WindowFactory;
import com.epamlab.beans.WoodenWindow;

public class MinskFactory implements WindowFactory {

    public static final String MINSK_FACTORY = "Minsk factory";

    @Override
    public WoodenWindow createWoodenWindow() {
        return new WoodenWindowMinsk();
    }

    @Override
    public SpecialWindow createSpecialWindow() {
        return new SpecialWindowMinsk();
    }

    @Override
    public PlasticWindow createPlasticWindow() {
        return new PlasticWindowMinsk();
    }

    @Override
    public String getName() {
        return MINSK_FACTORY;
    }
}
