package com.epamlab.factories;


import com.epamlab.beans.PlasticWindowGomel;
import com.epamlab.beans.SpecialWindowGomel;
import com.epamlab.beans.WoodenWindowGomel;
import com.epamlab.beans.PlasticWindow;
import com.epamlab.beans.SpecialWindow;
import com.epamlab.beans.WindowFactory;
import com.epamlab.beans.WoodenWindow;

public class GomelFactory implements WindowFactory {

    public static final String GOMEL_FACTORY = "Gomel factory";

    @Override
    public WoodenWindow createWoodenWindow() {
        return new WoodenWindowGomel();
    }

    @Override
    public SpecialWindow createSpecialWindow() {
        return new SpecialWindowGomel();
    }

    @Override
    public PlasticWindow createPlasticWindow() {
        return new PlasticWindowGomel();
    }

    @Override
    public String getName() {
        return GOMEL_FACTORY;
    }

}
