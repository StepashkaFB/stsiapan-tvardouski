package com.epamlab.beans;

public interface WindowFactory {
    WoodenWindow createWoodenWindow();
    SpecialWindow createSpecialWindow();
    PlasticWindow createPlasticWindow();
    String getName();
}
