package com.epamlab.beans;

public interface WoodenWindow extends Window {
    int costRatio = 3;
    String getInfoWindow();
}
