package com.epamlab.beans;

import static com.epamlab.constants.Constant.PLASTIC_WINDOW;
import static com.epamlab.constants.Constant.NAME_OF_WINDOW;
import static com.epamlab.constants.Constant.MATERIAL;
import static com.epamlab.constants.Constant.COST;

public class PlasticWindowGomel implements PlasticWindow {

    public static final String POLYVINYL_CHLORIDE = "Polyvinyl chloride";

    @Override
    public String getMaterial() {
        return POLYVINYL_CHLORIDE;
    }

    @Override
    public int getCost() {
        return 50 * costRatio;
    }

    @Override
    public String getNameWindow() {
        return PLASTIC_WINDOW;
    }

    @Override
    public String getInfoWindow() {
        return NAME_OF_WINDOW + getNameWindow()+ "\n" +
                MATERIAL + getMaterial() + "\n" +
                COST + getCost();
    }
}
