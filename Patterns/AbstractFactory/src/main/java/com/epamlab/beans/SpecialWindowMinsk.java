package com.epamlab.beans;

import static com.epamlab.constants.Constant.SPECIAL_WINDOW;
import static com.epamlab.constants.Constant.NAME_OF_WINDOW;
import static com.epamlab.constants.Constant.MATERIAL;
import static com.epamlab.constants.Constant.COST;

public class SpecialWindowMinsk implements SpecialWindow {
    @Override
    public String getMaterial() {
        return "Nickel";
    }

    @Override
    public int getCost() {
        return 150;
    }

    @Override
    public String getNameWindow() {
        return SPECIAL_WINDOW;
    }

    @Override
    public String getInfoWindow() {
        return NAME_OF_WINDOW + getNameWindow()+ "\n" +
                MATERIAL + getMaterial() + "\n" +
                COST + getCost();
    }
}
