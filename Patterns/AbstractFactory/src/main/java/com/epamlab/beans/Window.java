package com.epamlab.beans;

public interface Window {
    String getMaterial();
    int getCost();
    String getNameWindow();
}
