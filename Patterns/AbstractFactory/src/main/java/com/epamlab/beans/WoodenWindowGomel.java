package com.epamlab.beans;

import static com.epamlab.constants.Constant.WOODEN_WINDOW;
import static com.epamlab.constants.Constant.NAME_OF_WINDOW;
import static com.epamlab.constants.Constant.MATERIAL;
import static com.epamlab.constants.Constant.COST;

public class WoodenWindowGomel implements WoodenWindow {

    @Override
    public String getMaterial() {
        return "Oak";
    }

    @Override
    public int getCost() {
        return 10;
    }

    @Override
    public String getNameWindow() {
        return WOODEN_WINDOW;
    }

    @Override
    public String getInfoWindow() {
        return NAME_OF_WINDOW + getNameWindow()+ "\n" +
                MATERIAL + getMaterial() + "\n" +
                COST + getCost();
    }
}
