package com.epamlab.beans;

import static com.epamlab.constants.Constant.SPECIAL_WINDOW;
import static com.epamlab.constants.Constant.NAME_OF_WINDOW;
import static com.epamlab.constants.Constant.MATERIAL;
import static com.epamlab.constants.Constant.COST;

public class SpecialWindowGomel implements SpecialWindow {
    public static final String TITANIUM = "Titanium";

    @Override
    public String getMaterial() {
        return TITANIUM;
    }

    @Override
    public int getCost() {
        return 100 * costRatio;
    }

    @Override
    public String getNameWindow() {
        return SPECIAL_WINDOW;
    }

    @Override
    public String getInfoWindow() {
        return NAME_OF_WINDOW + getNameWindow()+ "\n" +
                MATERIAL + getMaterial() + "\n" +
                COST + getCost();
    }
}
