package com.epamlab.beans;

public interface SpecialWindow extends Window {
    int costRatio = 4;
    String getInfoWindow();
}
