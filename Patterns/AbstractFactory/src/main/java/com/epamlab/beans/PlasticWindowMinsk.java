package com.epamlab.beans;

import static com.epamlab.constants.Constant.PLASTIC_WINDOW;
import static com.epamlab.constants.Constant.NAME_OF_WINDOW;
import static com.epamlab.constants.Constant.MATERIAL;
import static com.epamlab.constants.Constant.COST;

public class PlasticWindowMinsk implements PlasticWindow {
    @Override
    public String getMaterial() {
        return "Polyvinyl chloride extra";
    }

    @Override
    public int getCost() {
        return 70/costRatio;
    }

    @Override
    public String getNameWindow() {
        return PLASTIC_WINDOW;
    }

    @Override
    public String getInfoWindow() {
        return NAME_OF_WINDOW + getNameWindow()+ "\n" +
                MATERIAL + getMaterial() + "\n" +
                COST + getCost();
    }
}
