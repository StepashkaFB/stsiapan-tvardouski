package com.epam;

import com.epam.beans.Sender;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "touch", defaultPhase = LifecyclePhase.PROCESS_SOURCES, threadSafe = true)
public class MailMojo extends AbstractMojo{
    @Parameter(property = "username")
    private String username;
    @Parameter(property = "password")
    private String password;
    @Parameter(property = "subject")
    private String subject;
    @Parameter(property = "text")
    private String text;
    @Parameter(property = "toEmail")
    private String toEmail;

    public void execute() throws MojoExecutionException {
        getLog().info("Start send message");
        Sender sender = new Sender(username,password);
        sender.send(subject,text,toEmail);
        getLog().info("End send message");
    }

}
