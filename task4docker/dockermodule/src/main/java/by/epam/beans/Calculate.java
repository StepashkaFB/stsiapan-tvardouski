package by.epam.beans;

public interface Calculate {
    boolean checkToMove(int value);
    int getSpeed();
}
