package by.epam.beans;


import by.epam.exceptions.DataIncorrectException;
import static by.epam.constants.Constant.COMMA;

/**
 * Class AbstractBug describes model of bugs
 * - implements Bug
 * @author Stsiapan Tvardouski
 */
public abstract class AbstractBug implements Bug {

    /**
     * This filed using for method toString()
     */
    public static final String STR = "\n";
    /**
     * This field using for console output
     */
    public static final String NAME_OF_BUG = "Name of bug: ";
    /**
     * This field using for console output
     */
    public static final String MASS_OF_BUG = "Mass of bug: ";
    /**
     * This field using for console output
     */
    public static final String MOVE = "Ready to move";
    /**
     * This field using for console output
     */
    public static final String STOP = "Stop";
    /**
     * This field using for console output
     */
    public static final String SUM_DISTANCE = "Sum distance: ";
    /**
     * This field using for console output
     */
    public static final String AMOUNT_OF_ENERGY = "Amount of energy: ";

    /**
     * This field contains information about name of bug
     */
    private final String name;
    /**
     * This field contains information about mass of bug
     */
    private int mass;
    /**
     * This field contains information about the distance traveled
     */
    private int distance;
    /**
     * This field contains information about the energy expended
     */
    private int amountOfEnergy;
    /**
     * This field contains information about the readiness to move
     */
    private boolean readyToMove;

    /**
     * Constructor
     * @param name - name of bug
     * @param mass - mass of bug
     */
    public AbstractBug(String name, int mass) {
        this.name = name;
        setMass(mass);
    }

    /**
     * get a power
     * @return a power of bug
     */
    @Override
    public int  powerConsumption(){
        return mass;
    }

    /**
     *This method describes basic start logic of bug
     */
    @Override
    public void runUp(){
        readyToMove = true;
        System.out.println(MOVE);
    }

    /**
     * This method describes basic shift logic of bug
     * @param distance - path that a fly must pass
     */
    @Override
    public void shift(int distance){
        if(distance <0){
            throw new DataIncorrectException(distance);
        }
    }
    /**
     * This method describes basic stop logic of bug
     */
    @Override
    public void stop(){
        readyToMove = false;
        System.out.println(STOP);
    }

    /**
     * getter {@link AbstractBug#readyToMove}
     * @return readyToMove
     */
    public boolean isReadyToMove() {
        return readyToMove;
    }

    /**
     * getter {@link AbstractBug#amountOfEnergy}
     * @return amount of energy
     */
    public int getAmountOfEnergy(){
        return  amountOfEnergy;
    }

    /**
     * getter {@link AbstractBug#mass}
     * @return mass of bug
     */
    public int getMass(){
        return mass;
    }

    /**
     * accumulator setter {@link AbstractBug#distance}
     * @param distance - path that a fly must pass
     */
    void setDistance(int distance){
        this.distance += distance;
    }

    /**
     * accumulator setter {@link AbstractBug#amountOfEnergy}
     * @param amountOfEnergy - amount of energy
     */
    void setAmountOfEnergy(int amountOfEnergy){
        this.amountOfEnergy +=amountOfEnergy;
    }

    /**
     * setter {@link AbstractBug#readyToMove}
     * @param readyToMove - willingness to move
     */
    public void setReadyToMove(boolean readyToMove) {
        this.readyToMove = readyToMove;
    }

    /**
     * setter with check logic {@link AbstractBug#mass}
     * @param mass - mass of bug
     */
    public void setMass(int mass) {
        if(mass <= 0) {
            throw new DataIncorrectException(mass);
        }
        this.mass = mass;
    }

    /**
     * Representation of the object as a string
     * @return object state
     */
    @Override
    public String toString() {
        return new StringBuilder().append(NAME_OF_BUG).append(name).append(COMMA).append(MASS_OF_BUG)
                .append(mass).append(COMMA).append(SUM_DISTANCE).append(distance)
                .append(COMMA).append(AMOUNT_OF_ENERGY).append(amountOfEnergy).append(COMMA).append(STR).toString();
    }
}
