package by.epam.beans;

public interface Movement {
    int getMass();
    int getPower();
}
