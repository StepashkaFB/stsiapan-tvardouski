package by.epam.beans;


import static by.epam.constants.Constant.DISTANCE_TRAVELED;

/**
 * Class SimpleFly describes a simple fly
 * - extends AbstractBug
 * @author Stsiapan Tvardouski
 */
public class SimpleFly extends AbstractBug {
    /**
     *This filed using for getting ratio mass
     */
    public static final int MASS = 100;
    /**
     * This field using for console output
     */
    public static final String NOT_READY_TO_MOVE = "Do not ready to move";
    /**
     * This field using for console output
     */
    public static final String ENERGY_EXPENDED = "Energy expended: ";
    /**
     *This field contains information about type of way to move
     */
    private final Movement wing;
    /**
     *Constructor
     * @param name - name of bug
     * @param mass - mass of bug
     * @param wing - type of wing
     */
    public SimpleFly(String name, int mass, Movement wing) {
        super(name, mass);
        this.wing = wing;
    }

    /**
     * getter {@link SimpleFly#wing}
     * @return - wing
     */
    public Movement getWing(){
        return wing;
    }

    /**
     *  This method calculate a ratio mass
     * @return - ratio mass
     */

    public int getRatioMass(){
        return getMass() + wing.getMass() <= MASS  ? 1:2;
    }

    /**
     * This method calculate power consumption
     * @return - power of fly
     */
    @Override
    public int  powerConsumption(){
        return super.powerConsumption() * wing.getPower() * getRatioMass();
    }

    /**
     * This method describes logic of shift of fly
     * @param distance - path that a fly must pass
     */
    @Override
    public void shift(int distance) {
        super.shift(distance);
        if(isReadyToMove()){
            int temp = distance * powerConsumption();
            setDistance(distance);
            setAmountOfEnergy(temp);
            System.out.println(DISTANCE_TRAVELED + distance);
            System.out.println(ENERGY_EXPENDED + getAmountOfEnergy());
        }else{
            System.out.println(NOT_READY_TO_MOVE);
        }
    }
    /**
     * Representation of the object as a string
     * @return object state
     */
    @Override
    public String toString() {
        return new StringBuilder(super.toString()).append(wing).toString();
    }
}
