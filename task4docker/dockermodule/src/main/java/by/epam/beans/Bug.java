package by.epam.beans;

public interface Bug {
    void runUp();
    void shift(int distance);
    void stop();
    int powerConsumption(); //speed * factor + mass

}
