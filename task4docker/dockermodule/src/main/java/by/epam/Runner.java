package by.epam;

import by.epam.beans.*;
import by.epam.exceptions.DataIncorrectException;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Runner demonstrates functional of application
 * @author Stsiapan Tvardouski
 */
public class Runner {
    public static void main(String[] args) {
        List<AbstractBug> bugs = new ArrayList<>();
        try {
            bugs.add(new SimpleFly("Fly", 20, new Wing("SimpleWing", 25, 4, 3)));
            bugs.add(new WeakFly("WeakFly", 15, new Wing("Wing", 10, 4, 5)));
            bugs.add(new ElectricFly("ElFly", 45, 15000, new Wing("ElWing", 15, 2, 4)));

            for (AbstractBug bug : bugs) {
                bug.runUp();
                bug.shift(25);
                bug.shift(50);
                bug.stop();
                System.out.println(bug);
            }
        }catch (DataIncorrectException e){
            System.out.println(e.getMessage());
        }
    }


}
