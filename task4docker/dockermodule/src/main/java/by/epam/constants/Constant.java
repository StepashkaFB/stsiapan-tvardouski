package by.epam.constants;

/**
 * Class Constant which contains constants
 * @author Stsiapan Tvardouski
 */
public class Constant {
    /**
     * This filed using for method toString()
     */
    public static final String COMMA = ";";
    /**
     * This field using for console output
     */
    public static final String DISTANCE_TRAVELED = "Distance traveled: ";
    private Constant(){}
}
