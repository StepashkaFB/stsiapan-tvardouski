import java.util.InputMismatchException;
import java.util.Scanner;

public class Task0 {
    private static void chooseAlg(int algorithmId, int loopType, int number){
        switch (algorithmId){
            case 1:
                fibonacci(loopType,number);
                break;
            case 2:
                System.out.println(factorial(loopType,number));
                break;
            default:
                System.out.println("Incorrect type of algorithmId: " + algorithmId);
        }
    }
    private static void fibonacci(int loopType, int number){
        if(number<1){
            System.out.println("Incorrect input: " + number);
            return;
        }
        if(number<=2){
            for(int counter = 1; counter <= number; counter++){
                System.out.print(1 + " ");
            }
        }else{
            int number_0 = 1;
            int number_1 = 1;
            int result;
            System.out.print(number_0 + " " + number_1 + " ");
            int counter = 3;
            switch (loopType){
                case 1:
                    while(counter++<=number){
                        result = number_0 + number_1;
                        System.out.print(result+" ");
                        number_0 = number_1;
                        number_1 = result;
                    }
                    System.out.println();
                    break;
                case 2:
                    do{
                        result = number_0 + number_1;
                        System.out.print(result + " ");
                        number_0 = number_1;
                        number_1 = result;
                    }while(counter++ < number);
                    System.out.println();
                    break;
                case 3:
                    for(counter = 3; counter <= number;counter++){
                        result = number_0 + number_1;
                        System.out.print(result + " ");
                        number_0= number_1;
                        number_1 = result;
                    }
                    System.out.println();
                    default:
                        System.out.println("Incorrect type of loop: " + loopType);
            }
        }
    }
    private static long factorial(int loopType, int n){
        if(n < 0){
            System.out.println("Incorrect input: " + n);
            return -1;
        }
        long result = 1;
        if (n == 0 || n == 1) {
            return result;
        }
        int counter = 1;
        switch (loopType) {
            case 1:
                while (counter <=n){
                    result *= counter;
                    counter++;
                }
                break;
            case 2:
                do{
                    result *= counter;
                    counter++;
                }while(counter<=n);
                break;
            case 3:
                for (counter=1; counter <= n; counter++) {
                    result *= counter;
                }
                break;
            default:
                System.out.println("Incorrect type of loop: " + loopType);
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while(true) {
            try {
                System.out.println("Enter type of algorithm:");
                int id = in.nextInt();
                System.out.println("Enter type of loop:");
                int loop = in.nextInt();
                System.out.println("Enter an amount of number:");
                int number = in.nextInt();
                chooseAlg(id, loop, number);
                System.out.println("Continue? y/n");
                if(in.next().equals("n")) {
                    return;
                }
            } catch (InputMismatchException e) {
                System.out.println("Wrong input: " + in.next());
            }
        }
    }
}
