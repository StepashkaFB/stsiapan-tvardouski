package com.epam.util;

import java.util.concurrent.RecursiveTask;

/**
 * The type Sum number.
 * - extends RecursiveTask<Long>
 *
 * @author Stsiapan Tvardouski
 */
public class SumNumber extends RecursiveTask<Long> {

    private final int start;
    private final int end;
    private static int counter = 0;

    /**
     * Instantiates a new Sum number.
     *
     * @param start the start
     * @param end   the end
     */
    public SumNumber(final int start, final int end) {
        ++counter;
        System.out.println(counter);
        this.start = start;
        this.end = end;
    }

    /**
     * Partitioning logic
     * @return result of calc sum number
     */
    @Override
    protected Long compute() {
        final long threshold = 2_000_000;
        if (end - start < threshold) {
            return computeDirectly();
        } else {
            final int middle = (end + start) / 2;
            final SumNumber subTask1 = new SumNumber(start, middle - 1);
            final SumNumber subTask2 = new SumNumber(middle, end);
            invokeAll(subTask1, subTask2);
            return subTask1.join() + subTask2.join();
        }
    }

    /**
     * Sum of number
     * @return sun of number in interval
     */
    private long computeDirectly() {
        long temp = 0;
        for (int i = start; i <= end; i++) {
            temp += i;
        }
        return temp;
    }
}
