package com.epam;

import com.epam.util.SumNumber;

import java.util.concurrent.ForkJoinPool;

public class Runner {
    public static void main(String[]args) {
        final long start = System.currentTimeMillis();
        final SumNumber task = new SumNumber(0,1_000_000_000);
        System.out.println(new ForkJoinPool(4).invoke(task));
        System.out.println(System.currentTimeMillis() - start);
        //withoutFork();
    }

    public static void withoutFork(){
        long temp = 0;
        final long start = System.currentTimeMillis();
        for (int i = 0; i <= 1_000_000_000; i++) {
            temp += i;
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}
