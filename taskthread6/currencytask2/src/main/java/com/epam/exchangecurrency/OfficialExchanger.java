package com.epam.exchangecurrency;

import com.epam.beans.Tourist;
import com.epam.util.Rate;
import com.epam.util.TimeWork;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Official exchanger.
 * - extends Exchanger
 * - implements Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class OfficialExchanger extends Exchanger implements Runnable {
    private static final String OFFICIAL_EXCHANGER = "Official Exchanger";
    private static final Logger LOG = Logger.getLogger(OfficialExchanger.class.getName());
    private static final String START_WORK = "Start work: ";
    private static final String RATE = "Rate = ";

    /**
     * Instantiates a new Official exchanger.
     *
     * @param exchange the exchange queue
     * @param rate     the rate of money
     */
    public OfficialExchanger(final ExchangeQueue exchange, final Rate rate) {
        super(exchange, rate);
    }

    /**
     * This method describes a logic of official exchange
     */
    @Override
    public void run() {
        Thread.currentThread().setName(OFFICIAL_EXCHANGER);
        LOG.info(START_WORK + Thread.currentThread().getName());
        int temp;
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                final Tourist tourist = exchange.get();
                temp = rate.getValue();
                LOG.info(RATE + temp);
                if (tourist != null) {
                    LOG.info(tourist.toString());
                    LOG.log(Level.SEVERE, OFFICIAL_EXCHANGER + " - Currency exchange occurred from: " + tourist.getValue() +
                            " to: " + tourist.getValue() * temp);
                }
                Thread.sleep(3000);
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
