package com.epam.exchangecurrency;

import com.epam.beans.Tourist;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

/**
 * The type Huckster exchange queue.
 *  - implements ExchangeQueue
 *
 * @author Stsiapan Tvardouski
 */
public class HucksterExchangeQueue implements ExchangeQueue {
    private static final Logger LOG = Logger.getLogger(HucksterExchangeQueue.class.getName());
    private static final String TOURIST_COME_IN_HUCKSTER_S_EXCHANGER
            = "Tourist come in huckster's exchanger:";
    private final Queue<Tourist> store = new ConcurrentLinkedQueue<>();

    /**
     * Add a new tourist in the queue
     * @param tourist the tourist
     */
    @Override
    public void add(final Tourist tourist) {
        store.add(tourist);
        LOG.info(TOURIST_COME_IN_HUCKSTER_S_EXCHANGER + tourist.getName());
    }

    /**
     * Get tourist from queue
     * @return Tourist
     */
    @Override
    public Tourist get() {
        return store.poll();
    }
}
