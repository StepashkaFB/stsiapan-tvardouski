package com.epam.exchangecurrency;

import com.epam.beans.Tourist;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Official exchange queue.
 * - implements ExchangeQueue
 *
 * @author Stsiapan Tvardouski
 */
public class OfficialExchangeQueue implements ExchangeQueue{
    private static final String TOURIST_COME_IN_OFFICIAL_EXCHANGER
            = "Tourist come in official exchanger:";
    private static final Logger LOG = Logger.getLogger(OfficialExchangeQueue.class.getName());

    private final BlockingQueue<Tourist> store = new ArrayBlockingQueue<>(2,true);

    /**
     * Add a new tourist in queue
     * @param tourist the new tourist
     */
    @Override
    public void add(final Tourist tourist) {
        try {
            store.put(tourist);
            LOG.info(TOURIST_COME_IN_OFFICIAL_EXCHANGER + tourist.getName());
        } catch (final InterruptedException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Get tourist from queue
     * @return Tourist
     */
    @Override
    public Tourist get() {
        Tourist tourist = null;
        try {
            tourist = store.poll(10, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
        return tourist;
    }
}
