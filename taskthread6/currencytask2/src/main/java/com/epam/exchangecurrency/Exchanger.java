package com.epam.exchangecurrency;

import com.epam.util.Rate;

/**
 * The type Exchanger.
 *
 * @author Stsiapan Tvardouski
 */
public abstract class Exchanger {
    /**
     * The Exchange.
     */
    ExchangeQueue exchange;
    /**
     * The Rate.
     */
    Rate rate;

    /**
     * Instantiates a new Exchanger.
     *
     * @param exchange the exchange queue
     * @param rate     the rate of money
     */
    Exchanger(final ExchangeQueue exchange, final Rate rate) {
        this.exchange = exchange;
        this.rate = rate;
    }
}
