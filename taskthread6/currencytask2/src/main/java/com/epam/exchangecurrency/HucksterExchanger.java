package com.epam.exchangecurrency;

import com.epam.beans.Tourist;
import com.epam.util.Rate;
import com.epam.util.TimeWork;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Huckster exchanger.
 * - extends {@link Exchanger}
 * - implements Runnable
 *
 *  @author Stsiapan Tvardouski
 */
public class HucksterExchanger extends Exchanger implements Runnable {
    private static final String HUCKSTER_EXCHANGER = "Huckster Exchanger";
    private static final Logger LOG = Logger.getLogger(HucksterExchanger.class.getName());
    private static final String HUCKSTER_IS_WAITING_A_NEW_TOURIST = "Huckster is waiting a new Tourist!";
    private static final String RATE = "Rate = ";
    private static final String START_WORK = "Start work: ";

    /**
     * Instantiates a new Huckster exchanger.
     *
     * @param exchange the exchange queue
     * @param rate     the rate of money
     */
    public HucksterExchanger(final ExchangeQueue exchange, final Rate rate) {
        super(exchange, rate);
    }

    /**
     *This method describes a logic of huckster exchange
     */
    @Override
    public void run() {
        Thread.currentThread().setName(HUCKSTER_EXCHANGER);
        LOG.info(START_WORK + Thread.currentThread().getName());
        int temp;
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                    Thread.sleep(1000);
                final Tourist tourist = exchange.get();
                temp = rate.getValue();
                if (tourist != null) {
                    LOG.info(RATE + temp);
                    LOG.info(tourist.toString());
                    LOG.log(Level.SEVERE, HUCKSTER_EXCHANGER + " - Currency exchange occurred from: " + tourist.getValue() +
                            " to: " + tourist.getValue() * temp * 0.95);
                } else {
                    Thread.sleep(2000);
                }
                LOG.log(Level.SEVERE, HUCKSTER_IS_WAITING_A_NEW_TOURIST);
            } catch (final InterruptedException e) {
                LOG.log(Level.INFO, e.getMessage());
            }
        }
    }
}
