package com.epam.util;

import java.time.LocalTime;

/**
 * The type Time work.
 *
 * @author Stsiapan Tvardouski
 */
public class TimeWork {
    /**
     * The constant localTimeNow.
     */
    public static final LocalTime localTimeNow = LocalTime.now();
    /**
     * The constant workTime.
     */
    public static final LocalTime workTime = localTimeNow.plusSeconds(40);
}
