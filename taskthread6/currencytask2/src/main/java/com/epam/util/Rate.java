package com.epam.util;

import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Rate.
 * - implements Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class Rate implements Runnable {
    private static final Logger log = Logger.getLogger(Rate.class.getName());
    private int startRate;
    private final int valueRate;
    private final ReentrantLock lock = new ReentrantLock();

    /**
     * Instantiates a new Rate.
     *
     * @param startRate the start rate
     */
    public Rate(final int startRate) {
        this.startRate = startRate;
        valueRate = startRate;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        lock.lock();
        final int temp = startRate;
        lock.unlock();
        return temp;
    }
    /**
     * This method contains a logic to set a new rate value
     * @param value - new rate value
     */
    private void setValue(final int value) {
        final int temp = getValue();
        if (temp + value <= 1) {
            startRate = temp - value;
        } else {
            startRate = temp + value;
        }
    }
    /**
     * This method change rate in the life time
     */
    @Override
    public void run() {
        final Random random = new Random();
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                final int temp = (int) (Math.random() * (valueRate * 2 + 1)) - valueRate;
                setValue(temp);
                Thread.sleep(5000 + 1000 * (1 + random.nextInt(5)));
            } catch (final InterruptedException e) {
                log.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
