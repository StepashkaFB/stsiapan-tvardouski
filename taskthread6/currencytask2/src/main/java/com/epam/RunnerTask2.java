package com.epam;

import com.epam.exchangecurrency.*;
import com.epam.util.Rate;
import com.epam.util.TouristGenerator;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunnerTask2 {

    private static final int START_RATE = 10;
    private static final int TOURIST_COUNT = 5;

    public static void main(final String[] args) {
        final ExchangeQueue officialExchangeQueue = new OfficialExchangeQueue();
        final ExchangeQueue hucksterExchangeQueue = new HucksterExchangeQueue();
        final Rate rate = new Rate(START_RATE);

        final OfficialExchanger officialExchanger = new OfficialExchanger(officialExchangeQueue, rate);
        final HucksterExchanger hucksterExchanger = new HucksterExchanger(hucksterExchangeQueue, rate);

        final TouristGenerator touristGenerator =
                new TouristGenerator(officialExchangeQueue, hucksterExchangeQueue, TOURIST_COUNT);
        final ExecutorService service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

        service.execute(rate);
        service.execute(officialExchanger);
        service.execute(hucksterExchanger);
        service.execute(touristGenerator);
        service.shutdown();


    }
}
