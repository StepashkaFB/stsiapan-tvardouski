package com.epam.beans;

import java.util.Random;

/**
 * Class Tourist describes a model tourist
 *
 * @author Stsiapan Tvardouski
 */
public class Tourist {
    /**
     * the name of tourist
     */
    private String name;
    /**
     * tourist's money
     */
    private int value;

    /**
     * Instantiates a new Tourist.
     *
     * @param name the name
     */
    public Tourist(final String name) {
        this.name = name;
        this.value = 100 + (1 + new Random().nextInt(900));
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(final int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Tourist{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
