package com.epam.util;

import com.epam.beans.Tourist;
import com.epam.exchangecurrency.ExchangeQueue;

import java.util.Random;

/**
 * The type Tourist generator.
 * - implements Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class TouristGenerator implements Runnable {
    private final ExchangeQueue exchangeOfficial;
    private final ExchangeQueue exchangeHuckster;
    private final int touristCount;

    /**
     * Instantiates a new Tourist generator.
     *
     * @param exchangeOfficial the exchange official
     * @param exchangeHuckster the exchange huckster
     * @param touristCount     the tourist count
     */
    public TouristGenerator(final ExchangeQueue exchangeOfficial, final ExchangeQueue exchangeHuckster, final int touristCount) {
        this.exchangeOfficial = exchangeOfficial;
        this.exchangeHuckster = exchangeHuckster;
        this.touristCount = touristCount;
    }

    /**
     * This method describe logic of generating tourists,
     * and adding this one to queue
     */
    @Override
    public void run() {
        int count = 0;
        final Random random = new Random();
        while (count < touristCount) {
            Thread.currentThread().setName(" Tourist Generator");
            count++;
            final Tourist tourist = new Tourist("Tourist№: " + count);
            if (count % 2 == 0) {
                exchangeOfficial.add(tourist);
            } else {
                exchangeHuckster.add(tourist);
            }
            try {
                Thread.sleep(25 * (1 + random.nextInt(10)));
            } catch (final InterruptedException ignored) {

            }
        }
    }
}
