package com.epam.util;

import java.time.LocalTime;

/**
 * The type Time work.
 *
 * @author Stsiapan Tvardouski
 */
public class TimeWork {
    private static final LocalTime localTimeNow = LocalTime.now();
    /**
     * The constant workTime.
     */
    public static final LocalTime workTime = localTimeNow.plusSeconds(30);

    /**
     * private constructor
     */
    private TimeWork() {
    }
}
