package com.epam.exchangecurrency;

import com.epam.beans.Tourist;
import com.epam.util.Rate;
import com.epam.util.TimeWork;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Official exchanger.
 * - extends Exchanger
 * - implements Runnable
 */
public class OfficialExchanger extends Exchanger implements Runnable {
    private static final String OFFICIAL_EXCHANGER = "Official Exchanger";
    private static final Logger LOG = Logger.getLogger(OfficialExchanger.class.getName());
    private static final String RATE = "Rate = ";
    private static final String START_WORK = "Start work: ";

    /**
     * Instantiates a new Official exchanger.
     *
     * @param exchange the exchange
     * @param rate     the rate
     */
    public OfficialExchanger(final ExchangeQueue exchange, final Rate rate) {
        super(exchange, rate);
    }

    /**
     * This method describes a logic of official exchange
     */
    @Override
    public void run() {
        Thread.currentThread().setName(OFFICIAL_EXCHANGER);
        LOG.info(START_WORK + Thread.currentThread().getName());
        int temp;

        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                Thread.sleep(3000);
                final Tourist tourist = exchange.get();
                temp = rate.getValue();
                LOG.info(RATE + temp);
                if (tourist != null) {
                    LOG.info(tourist.toString());
                    LOG.log(Level.SEVERE, OFFICIAL_EXCHANGER + " - Произошел обмен валюты с: " + tourist.getValue() +
                            " по: " + tourist.getValue() * temp);
                }
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
