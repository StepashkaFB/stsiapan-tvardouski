package com.epam.exchangecurrency;

import com.epam.beans.Tourist;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Huckster exchange queue.
 * - implements {@link ExchangeQueue}
 * @author Stsiapan Tvardouski
 */
public class HucksterExchangeQueue implements ExchangeQueue {
    private static final String TOURIST_COME_IN_HUCKSTER_S_EXCHANGER
            = "Tourist come in huckster's exchanger:";
    private static final String THERE_ARE_NO_TOURIST_IN_THE_HUCKSTER_S_EXCHANGER
            = "There are no tourist in the huckster's exchanger";
    private static final Logger LOG = Logger.getLogger(HucksterExchangeQueue.class.getName());
    private final List<Tourist> store = new ArrayList<>();

    /**
     * This method add the new tourist in list of tourist
     * @param tourist the new tourist
     */
    @Override
    public synchronized void add(final Tourist tourist) {
        try {
            notify();
            store.add(tourist);
            LOG.info(TOURIST_COME_IN_HUCKSTER_S_EXCHANGER + tourist.getName());
        } catch (final Exception e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }

    }

    /**
     * Get tourist
     * @return Tourist from list or null
     */
    @Override
    public synchronized Tourist get() {
        try {
            if (!store.isEmpty()) {
                return store.remove(0);
            }
            LOG.info(THERE_ARE_NO_TOURIST_IN_THE_HUCKSTER_S_EXCHANGER);
            wait(6000);
        } catch (final Exception e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }
}
