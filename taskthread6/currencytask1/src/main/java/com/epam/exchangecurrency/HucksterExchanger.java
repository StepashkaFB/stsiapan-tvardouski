package com.epam.exchangecurrency;

import com.epam.beans.Tourist;
import com.epam.util.Rate;
import com.epam.util.TimeWork;

import java.time.LocalTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Huckster exchanger.
 * - extends {@link Exchanger}
 * - implements Runnable
 * @author Stsiapan Tvardouski
 */
public class HucksterExchanger extends Exchanger implements Runnable {
    private static final Logger LOG = Logger.getLogger(HucksterExchanger.class.getName());
    private static final String HUCKSTER_EXCHANGER = "Huckster Exchanger";
    private static final String START_WORK = "Start work: ";
    private static final String RATE = "Rate = ";

    /**
     * Instantiates a new Huckster exchanger.
     *
     * @param exchange the exchange
     * @param rate     the rate
     */
    public HucksterExchanger(final ExchangeQueue exchange, final Rate rate) {
        super(exchange, rate);
    }

    /**
     * This method describes a logic of huckster exchange
     */
    @Override
    public void run() {
        Thread.currentThread().setName(HUCKSTER_EXCHANGER);
        LOG.info(START_WORK + Thread.currentThread().getName());
        int temp;
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                Thread.sleep(20);
                final Tourist tourist = exchange.get();
                temp = rate.getValue();
                LOG.info(RATE + temp);
                if (tourist != null) {
                    LOG.info(tourist.toString());
                    LOG.log(Level.SEVERE, HUCKSTER_EXCHANGER + " - Произошел обмен валюты с: " + tourist.getValue() +
                            " по: " + tourist.getValue() * temp * 0.95);
                }
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
    }
}
