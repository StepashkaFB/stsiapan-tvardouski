package com.epam.exchangecurrency;

import com.epam.beans.Tourist;

/**
 * The interface Exchange queue.
 *
 * @author Stsiapan Tvardouski
 */
public interface ExchangeQueue {
    /**
     * Add a new tourist
     *
     * @param tourist the tourist
     */
    void add(Tourist tourist);

    /**
     * Get tourist.
     *
     * @return the tourist
     */
    Tourist get();
}
