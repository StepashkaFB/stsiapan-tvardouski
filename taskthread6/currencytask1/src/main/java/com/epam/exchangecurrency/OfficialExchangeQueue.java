package com.epam.exchangecurrency;

import com.epam.beans.Tourist;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Official exchange queue.
 *  - implements {@link ExchangeQueue}
 * @author Stsiapan Tvardouski
 */
public class OfficialExchangeQueue implements ExchangeQueue {
    private static final String THERE_IS_NO_PLACE_FOR_A_TOURIST_IN_OFFICIAL_EXCHANGER
            = "There is no place for a tourist in official exchanger";
    private static final String TOURIST_COME_IN_OFFICIAL_EXCHANGER
            = "Tourist come in official exchanger:";
    private static final String THERE_ARE_NO_TOURIST_IN_THE_OFFICIAL_EXCHANGER
            = "There are no tourist in the official exchanger";

    private static final Logger LOG = Logger.getLogger(OfficialExchangeQueue.class.getName());
    private static final int MAX_TOURIST_IN_QUERY = 3;
    private static final int MIN_TOURIST_IN_QUERY = 0;
    private int touristsCounter = 0;
    private final List<Tourist> store = new ArrayList<>();

    /**
     * Add a new tourist in list
     * @param tourist the new tourist
     */
    public synchronized void add(final Tourist tourist) {
        while (touristsCounter > MAX_TOURIST_IN_QUERY - 1) {
            try {
                LOG.info(THERE_IS_NO_PLACE_FOR_A_TOURIST_IN_OFFICIAL_EXCHANGER);
                wait();
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
        store.add(tourist);
        LOG.info(TOURIST_COME_IN_OFFICIAL_EXCHANGER + tourist.getName());
        touristsCounter++;
        notify();
    }

    /**
     * Get tourist from list or null
     * @return Tourist
     */
    public synchronized Tourist get() {
        try {
            if (touristsCounter > MIN_TOURIST_IN_QUERY) {
                notify();
                touristsCounter--;
                return store.remove(0);
            }
            LOG.info(THERE_ARE_NO_TOURIST_IN_THE_OFFICIAL_EXCHANGER);
            wait(6000);

        } catch (final InterruptedException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }
}
