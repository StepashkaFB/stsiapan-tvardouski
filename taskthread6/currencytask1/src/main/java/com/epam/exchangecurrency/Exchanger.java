package com.epam.exchangecurrency;

import com.epam.util.Rate;

/**
 * The type Exchanger.
 *
 * @author Stsiapan Tvardouski
 */
abstract class Exchanger {
    /**
     * The Exchange queue.
     */
    ExchangeQueue exchange;
    /**
     * The Rate of money.
     */
    Rate rate;

    /**
     * Instantiates a new Exchanger.
     *
     * @param exchange the exchange
     * @param rate     the rate
     */
    Exchanger(final ExchangeQueue exchange, final Rate rate) {
        this.exchange = exchange;
        this.rate = rate;
    }
}
