package com.epam;

import com.epam.exchangecurrency.*;
import com.epam.util.Rate;
import com.epam.util.TouristGenerator;


public class Test {
    /**
     *  amount of tourist, 5 - was using for docker
     */
    private static final int TOURIST_COUNT = 5;

    public static void main(final String[] args) {
        final ExchangeQueue officialExchangeQueue = new OfficialExchangeQueue();
        final ExchangeQueue hucksterExchangeQueue = new HucksterExchangeQueue();
        final TouristGenerator touristGenerator =
                new TouristGenerator(officialExchangeQueue, hucksterExchangeQueue, TOURIST_COUNT);

        final Rate rate = new Rate(10);
        final OfficialExchanger officialExchanger = new OfficialExchanger(officialExchangeQueue, rate);
        final HucksterExchanger hucksterExchanger = new HucksterExchanger(hucksterExchangeQueue, rate);

        final Thread rateThread = new Thread(rate);
        final Thread threadGenerator = new Thread(touristGenerator);
        final Thread threadOfficial = new Thread(officialExchanger);
        final Thread threadHuckster = new Thread(hucksterExchanger);

        rateThread.start();
        threadOfficial.start();
        threadHuckster.start();
        threadGenerator.start();


    }
}
