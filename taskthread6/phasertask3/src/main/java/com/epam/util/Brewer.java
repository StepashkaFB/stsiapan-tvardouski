package com.epam.util;

import com.epam.beans.Place;
import com.epam.beans.PlaceQueue;
import com.epam.beans.Tourist;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Phaser;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Brewer.
 * - extends Place
 * - implements PlaceQueue, Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class Brewer extends Place implements PlaceQueue, Runnable {
    private static final Logger LOG = Logger.getLogger(Brewer.class.getName());
    private static final String TOURIST_COME_IN_BEER_PUB
            = "Tourist come in beer pub:";
    private static final String DRINK_BEER_ON_PHASE = " -- drink beer on phase ";
    private final Queue<Tourist> store = new ConcurrentLinkedQueue<>();

    /**
     * Instantiates a new Brewer.
     *
     * @param amount the amount of tourist
     * @param phaser the phaser
     */
    Brewer(final int amount, final Phaser phaser) {
        super(amount, phaser);
    }

    /**
     * Add a new tourist in queue
     * @param tourist the tourist
     */
    @Override
    public void add(final Tourist tourist) {
        store.add(tourist);
        LOG.info(TOURIST_COME_IN_BEER_PUB + tourist.getName());
    }

    /**
     * Get tourist from queue
     * @return tourist
     */
    @Override
    public Tourist get() {
        return store.poll();
    }

    /**
     * This method describe a life cycle of brewer
     */
    @Override
    public void run() {
        Thread.currentThread().setName(Brewer.class.getSimpleName());
        int counter = 0;
        phaser.register();
        while (counter < amount) {
            try {
                Thread.sleep(100);
                final Tourist tourist = get();
                if (tourist != null) {
                    tourist.increasePhotoCounter();
                    LOG.info(tourist.getName() + DRINK_BEER_ON_PHASE + phaser.getPhase());
                    Thread.sleep(tourist.getTimeEat());
                    counter++;
                }
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
        phaser.arriveAndDeregister();
    }
}
