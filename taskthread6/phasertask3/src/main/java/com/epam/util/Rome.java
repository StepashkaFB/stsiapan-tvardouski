package com.epam.util;

import java.util.concurrent.CyclicBarrier;
import java.util.logging.Logger;

/**
 * The type Rome.
 *
 * @author Stsiapan Tvardouski
 */
public class Rome {

    private static final Logger LOG = Logger.getLogger(Rome.class.getName());
    /**
     * The constant BARRIER_PIZZA.
     */
    public static final CyclicBarrier BARRIER_PIZZA = new CyclicBarrier(3, new ServicePizza());
    /**
     * The constant BARRIER_PASTA.
     */
    public static final CyclicBarrier BARRIER_PASTA = new CyclicBarrier(3, new ServicePasta());

    /**
     * private constructor
     */
    private Rome() {
    }

    /**
     * The type Service pizza.
     */
    public static class ServicePizza implements Runnable {

        /**
         * The Pizzeria served tourists.
         */
        static final String PIZZERIA_SERVED_TOURISTS_ = "Pizzeria served tourists ";

        /**
         * Served logic
         */
        @Override
        public void run() {
            try {
                Thread.sleep(200);
                LOG.info(PIZZERIA_SERVED_TOURISTS_);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * The type Service pasta.
     */
    public static class ServicePasta implements Runnable {

        /**
         * The Pasta restaurant serves tourists.
         */
        static final String PASTA_RESTAURANT_SERVES_TOURISTS = "Pasta restaurant serves tourists";

        /**
         * Served logic
         */
        @Override
        public void run() {
            try {
                Thread.sleep(200);
                LOG.info(PASTA_RESTAURANT_SERVES_TOURISTS);
            } catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
