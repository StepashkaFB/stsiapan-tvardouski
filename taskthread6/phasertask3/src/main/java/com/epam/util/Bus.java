package com.epam.util;

import com.epam.beans.Tourist;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.epam.constants.BusConstant.*;

/**
 * The type Bus.
 *
 * @author Stsiapan Tvardouski
 */
public class Bus implements Runnable {

    private static final Logger LOG = Logger.getLogger(Bus.class.getName());
    private List<Tourist> touristList;
    private final Phaser phaser;
    private final int amount;
    private final Brewer brewer;
    private final Paris paris;
    private final Semaphore semaphore = new Semaphore(1);

    /**
     * Instantiates a new Bus.
     *
     * @param amount the amount of tourist
     * @param phaser the phaser
     */
    public Bus(final int amount, final Phaser phaser) {
        this.amount = amount;
        this.phaser = phaser;
        brewer = new Brewer(amount, phaser);
        paris = new Paris(amount, phaser);
        init();
    }

    /**
     * Initialization a list of tourist, and set a start place, which they will visit
     */
    private void init() {
        touristList = new ArrayList<>();
        for (int i = 1; i <= amount; i++) {
            touristList.add(new Tourist(phaser, TOURIST + i, i));
        }
        touristList.forEach(e -> e.setPlaceQueue(brewer));
    }

    /**
     * Starting threads of tourists
     */
    private void start() {
        final ExecutorService service = Executors.newFixedThreadPool(40);
        touristList.forEach(service::execute);
        service.shutdown();
    }

    /**
     * The type Exchange cloud.
     */
    public static class ExchangeCloud {
        /**
         * The constant ATOMIC_INTEGER_PHOTO.
         */
        final static AtomicInteger ATOMIC_INTEGER_PHOTO = new AtomicInteger();
        /**
         * The constant INTEGER_EXCHANGER.
         */
        public final static Exchanger<Integer> INTEGER_EXCHANGER = new Exchanger<>();

        /**
         * private constructor
         */
        private ExchangeCloud() {
        }
    }
    /**
     * This method describe a life cycle of Bus
     */
    @Override
    public void run() {
        Thread.currentThread().setName(Bus.class.getSimpleName());
        phaser.register();
        start();
        LOG.info(BUS_IS_WAITING);

        phaser.arriveAndAwaitAdvance();
        new Thread(brewer).start();

        phaser.arriveAndAwaitAdvance();
        touristList.forEach(e -> e.setPlaceQueue(paris));
        new Thread(paris).start();

        phaser.arriveAndAwaitAdvance();
        //Rome phase
        phaser.arriveAndAwaitAdvance();
        touristList.forEach(e -> e.setSemaphore(semaphore));
        try {
            while (Thread.currentThread().isAlive()) {
                Thread.sleep(1000);
                ExchangeCloud.ATOMIC_INTEGER_PHOTO.addAndGet(ExchangeCloud.INTEGER_EXCHANGER.exchange(
                        null, 1000, TimeUnit.MILLISECONDS));
            }
        } catch (final InterruptedException | TimeoutException e) {
            LOG.log(Level.SEVERE, e.getMessage() + THERE_ARE_NOT_ANY_TOURIST_PHOTOS);
        }
        LOG.info(ExchangeCloud.ATOMIC_INTEGER_PHOTO.toString() + PHOTOS_ARE_IN_THE_CLOUD);
        phaser.arriveAndDeregister();
    }
}
