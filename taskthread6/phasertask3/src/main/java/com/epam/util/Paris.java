package com.epam.util;

import com.epam.beans.Place;
import com.epam.beans.PlaceQueue;
import com.epam.beans.Tourist;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Paris.
 * - extends Place
 * - implements PlaceQueue, Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class Paris extends Place implements PlaceQueue, Runnable {
    private static final Logger LOG = Logger.getLogger(Paris.class.getName());
    private static final String TOURIST_COME_INTO_THE_LOUVRE_MUSEUM = "Tourist come into the louvre museum: ";
    private static final String THE_LOUVRE_MUSEUM_ON_PHASE = " -- THE louvre museum on phase ";
    private final BlockingQueue<Tourist> store = new ArrayBlockingQueue<>(2,true);

    /**
     * Instantiates a new Paris.
     *
     * @param amount the amount of tourist
     * @param phaser the phaser
     */
    Paris(final int amount, final Phaser phaser) {
        super(amount, phaser);
    }

    /**
     * Add a new tourist in queue
     * @param tourist the tourist
     */
    @Override
    public void add(final Tourist tourist) {
        try {
            store.put(tourist);
            LOG.info(TOURIST_COME_INTO_THE_LOUVRE_MUSEUM + tourist.getName());
        } catch (final InterruptedException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Get a tourist from queue
     * @return Tourist or null
     */
    @Override
    public Tourist get() {
        Tourist tourist = null;
        try {
            tourist = store.poll(10, TimeUnit.SECONDS);
        } catch (final InterruptedException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
        return tourist;
    }

    /**
     * This method describe a logic of life cycle of paris
     */
    @Override
    public void run() {
        Thread.currentThread().setName(Paris.class.getSimpleName());
        int counter = 0;
        phaser.register();
        while (counter < amount) {
            try {
                Thread.sleep(100);
                final Tourist tourist = get();
                if (tourist != null) {
                    tourist.increasePhotoCounter();
                    LOG.info(tourist.getName() + THE_LOUVRE_MUSEUM_ON_PHASE + phaser.getPhase());
                    Thread.sleep(1000);
                    counter++;
                }
            } catch (final InterruptedException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
        phaser.arriveAndDeregister();
    }
}
