package com.epam.beans;

import com.epam.util.Bus;
import com.epam.util.Rome;

import java.util.Random;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.epam.constants.TouristConstant.*;

/**
 * The type Tourist.
 * - implements Runnable
 *
 * @author Stsiapan Tvardouski
 */
public class Tourist implements Runnable {
    private static final Logger LOG = Logger.getLogger(Tourist.class.getName());
    private final Phaser phaser;
    private final int id;
    private String name;
    private boolean isInTravel;
    private PlaceQueue placeQueue;
    private int photoCounter;
    private final int timeEat;
    private Semaphore semaphore;

    /**
     * Sets place queue.
     *
     * @param placeQueue the place queue
     */
    public void setPlaceQueue(final PlaceQueue placeQueue) {
        this.placeQueue = placeQueue;
    }


    /**
     * Instantiates a new Tourist.
     *
     * @param phaser the phaser
     * @param name   the name of tourist
     * @param id     the id of tourist
     */
    public Tourist(final Phaser phaser, final String name, final int id) {
        final Random random = new Random();
        this.phaser = phaser;
        this.name = name;
        this.id = id;
        timeEat = 1000 * (1 + random.nextInt(5));
    }

    /**
     * Increase photo counter.
     */
    public void increasePhotoCounter() {
        photoCounter++;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name of tourist
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets time eat.
     *
     * @return the time eat of tourist
     */
    public int getTimeEat() {
        return timeEat;
    }

    /**
     * Is in travel boolean.
     *
     * @return the boolean
     */
    public boolean isInTravel() {
        return isInTravel;
    }

    /**
     * Gets id.
     *
     * @return the id of tourist
     */
    public int getId() {
        return id;
    }

    /**
     * Gets photo counter.
     *
     * @return the photo counter of tourist
     */
    public int getPhotoCounter() {
        return photoCounter;
    }

    /**
     * Sets semaphore.
     *
     * @param semaphore the semaphore using for exchanging photos
     */
    public void setSemaphore(final Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    /**
     * Using for thread sleep
     * @param value - the sleep time
     */
    private void sleep(final int value) {
        try {
            Thread.sleep(value);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method describe a life cycle of tourist
     */
    @Override
    public void run() {
        phaser.register();
        LOG.info(name + IS_READY + phaser.getPhase());
        isInTravel = true;
        phaser.arriveAndAwaitAdvance();

        LOG.info(BEER_START_PHASE + phaser.getPhase() + name);
        sleep(2000);
        placeQueue.add(this);
        phaser.arriveAndAwaitAdvance();

        LOG.info(PARIS_START_PHASE + phaser.getPhase() + name);
        sleep(2000);
        placeQueue.add(this);
        phaser.arriveAndAwaitAdvance();

        LOG.info(ROME_START_PHASE + phaser.getPhase());
        increasePhotoCounter();
        if (id % 2 == 0) {
            try {
                LOG.info(name + IS_GOING_TO_PIZZA_RESTAURANT);
                Rome.BARRIER_PIZZA.await();
                sleep(timeEat);
                LOG.info(name + ATE_IN_PIZZA_RESTAURANT);
            } catch (final InterruptedException | BrokenBarrierException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        } else {
            LOG.info(name + IS_GOING_TO_PASTA_RESTAURANT);
            try {
                Rome.BARRIER_PASTA.await();
                sleep(timeEat);
                LOG.info(name + ATE_IN_PASTA_RESTAURANT);
            } catch (final InterruptedException | BrokenBarrierException e) {
                LOG.log(Level.SEVERE, e.getMessage());
            }
        }
        phaser.arriveAndAwaitAdvance();

        increasePhotoCounter();
        LOG.info(SPAIN_START_PHASE + phaser.getPhase());
        sleep((new Random().nextInt(5) + 1) * 1000);
        try {
            semaphore.acquire();
            final Exchanger<Integer> ex = Bus.ExchangeCloud.INTEGER_EXCHANGER;
            ex.exchange(photoCounter, 2500, TimeUnit.MILLISECONDS);
            photoCounter = 0;
            semaphore.release();
        } catch (final InterruptedException | TimeoutException e) {
            LOG.log(Level.SEVERE, e.getMessage());
        }
        isInTravel = false;
        phaser.arriveAndDeregister();
    }
}
