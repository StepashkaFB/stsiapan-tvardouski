package com.epam.beans;

/**
 * The interface Place queue.
 *
 * @author Stsiapan Tvardouski
 */
public interface PlaceQueue {
    /**
     * Add a new tourist.
     *
     * @param tourist the tourist
     */
    void add(Tourist tourist);

    /**
     * Get tourist.
     *
     * @return the tourist
     */
    Tourist get();
}
