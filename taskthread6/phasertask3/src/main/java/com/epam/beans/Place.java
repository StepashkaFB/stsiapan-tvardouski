package com.epam.beans;

import java.util.concurrent.Phaser;

/**
 * The type Place.
 *
 * @author Stsiapan Tvardouski
 */
public abstract class Place {
    /**
     * The Amount of people.
     */
    protected final int amount;
    /**
     * The Phaser.
     */
    protected final Phaser phaser;

    /**
     * Instantiates a new Place.
     *
     * @param amount the amount
     * @param phaser the phaser
     */
    protected Place(final int amount, final Phaser phaser) {
        this.amount = amount;
        this.phaser = phaser;
    }
}
