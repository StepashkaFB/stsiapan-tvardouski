package com.epam.constants;

/**
 * The type Bus constant.
 *
 * @author Stsiapan Tvardouski
 */
public class BusConstant {
    private BusConstant(){}

    /**
     * The constant THERE_ARE_NOT_ANY_TOURIST_PHOTOS.
     */
    public static final String THERE_ARE_NOT_ANY_TOURIST_PHOTOS = " there are not any tourist photos ";
    /**
     * The constant TOURIST.
     */
    public static final String TOURIST = "Tourist# - ";
    /**
     * The constant BUS_IS_WAITING.
     */
    public static final String BUS_IS_WAITING = "Bus is waiting";
    /**
     * The constant PHOTOS_ARE_IN_THE_CLOUD.
     */
    public static final String PHOTOS_ARE_IN_THE_CLOUD = " photos are in the cloud";
}
