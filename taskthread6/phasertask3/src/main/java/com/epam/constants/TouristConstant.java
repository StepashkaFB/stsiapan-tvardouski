package com.epam.constants;

/**
 * The type Tourist constant.
 *
 * @author Stsiapan Tvardouski
 */
public class TouristConstant {
    private TouristConstant() {}

    /**
     * The constant BEER_START_PHASE.
     */
    public static final String BEER_START_PHASE = "BEER Start Phase = ";
    /**
     * The constant IS_READY.
     */
    public static final String IS_READY = " -- is ready -";
    /**
     * The constant PARIS_START_PHASE.
     */
    public static final String PARIS_START_PHASE = "PARIS Start Phase = ";
    /**
     * The constant ROME_START_PHASE.
     */
    public static final String ROME_START_PHASE = "ROME Start Phase: ";
    /**
     * The constant IS_GOING_TO_PIZZA_RESTAURANT.
     */
    public static final String IS_GOING_TO_PIZZA_RESTAURANT = " is going to Pizza restaurant";
    /**
     * The constant ATE_IN_PIZZA_RESTAURANT.
     */
    public static final String ATE_IN_PIZZA_RESTAURANT = " ate in PIZZA restaurant";
    /**
     * The constant IS_GOING_TO_PASTA_RESTAURANT.
     */
    public static final String IS_GOING_TO_PASTA_RESTAURANT = " is going to Pasta restaurant";
    /**
     * The constant ATE_IN_PASTA_RESTAURANT.
     */
    public static final String ATE_IN_PASTA_RESTAURANT = " ate in PASTA restaurant";
    /**
     * The constant SPAIN_START_PHASE.
     */
    public static final String SPAIN_START_PHASE = "SPAIN start phase: ";
}
