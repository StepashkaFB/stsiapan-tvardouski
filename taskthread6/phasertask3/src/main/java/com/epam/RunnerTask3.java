package com.epam;

import com.epam.util.Bus;

import java.util.concurrent.Phaser;

public class RunnerTask3 {

    private static final int AMOUNT = 6;
    private static final int PARTIES = 1;

    public static void main(final String[] args) throws InterruptedException {
        Phaser phaser;
        Bus bus;
        for (int i = 0; i <=1; i++){
            phaser = new Phaser(PARTIES);
            bus = new Bus(AMOUNT,phaser);
            new Thread(bus).start();
            Thread.sleep(1000);
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndAwaitAdvance();
            phaser.arriveAndDeregister();
            Thread.sleep(5000);
        }
    }
}
