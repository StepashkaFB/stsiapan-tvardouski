import com.epam.beans.Pressure;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.groupingBy;

/**
 * @author Stepan Tvardovski
 */
public class Runner {
    /**
     * logger
     */
    private static Logger log = Logger.getLogger(Runner.class.getName());
    /**
     * minimum pressure
     */
    private static final int START_RANGE = 100;
    /**
     * maximum pressure
     */
    private static final int END_RANGE = 851;
    /**
     * amount days in the year
     */
    private static final int AMOUNT_OF_DAYS = 365;

    /**
     * This method fill the list
     * @param pressureList - list of pressure that
     *                     necessary to fill
     */
    private static void fillList(List<Pressure> pressureList) {
        IntStream.rangeClosed(0, AMOUNT_OF_DAYS)
                .forEach(i -> pressureList.add(new Pressure(getRandomNumberInRange(),
                        LocalDate.now().plus(i, ChronoUnit.DAYS))));
    }

    /**
     * get random number in a range
     * @return - random number
     */
    private static int getRandomNumberInRange() {
        Random random = new Random();
        return random.ints(START_RANGE, END_RANGE).limit(1).findFirst().orElse(START_RANGE);
    }

    /**
     * Get a minimum pressure from list
     * @param pressureList - list of pressure value
     * @return - return pressure
     */
    private static Pressure getMin(List<Pressure> pressureList) {
        return pressureList.stream().min(Comparator.comparingInt(Pressure::getValue)).get();
    }

    /**
     * Get a maximum pressure from list
     * @param pressureList - list of pressure value
     * @return - return pressure
     */
    private static Pressure getMax(List<Pressure> pressureList) {
        return pressureList.stream().max(Comparator.comparingInt(Pressure::getValue)).get();
    }
    /**
     * Get a average pressure from list
     * @param pressureList - list of pressure value
     * @return - return pressure
     */
    private static Double getAverage(List<Pressure> pressureList) {
        return pressureList.stream().mapToInt(Pressure::getValue).average().getAsDouble();
    }

    /**
     * This method shows information about
     * a min, max and avg pressure by month
     * @param pressureList - list of pressure value
     */
    private static void showMonth(List<Pressure> pressureList) {
        Map<LocalDate, List<Pressure>> map = pressureList.stream()
                .collect(groupingBy(d -> d.getLocalDate().withDayOfMonth(1)));
        map.forEach((K, V) -> log.info(K.getMonth() + " - MIN - " + getMin(V).getValue()));
        map.forEach((K, V) -> log.info(K.getMonth() + " - MAX - " + getMax(V).getValue()));
        map.forEach((K, V) -> log.info(K.getMonth() + " - AVG - " + getAverage(V)));
    }

    /**
     * This method create a collector
     *  that shows a maximum pressure in every month
     *  and after sorting values
     * @param pressureList - list of pressure value
     */
    private static void createCollector(List<Pressure> pressureList) {
        pressureList.parallelStream()
                .collect(Collectors.toMap(e -> e.getLocalDate().getMonth(), Function.identity(),
                        BinaryOperator.maxBy(Comparator.comparing(Pressure::getValue))))
                .entrySet()
                .stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .forEach(x -> log.info(x.getKey() + "--" + x.getValue()));
    }

    /**
     * Demonstrate a logic
     * @param args - string args
     */
    public static void main(String[] args) {
        List<Pressure> pressureList = new ArrayList<>();
        fillList(pressureList);
        log.info("Min: " + getMin(pressureList));
        log.info("Max: " + getMax(pressureList));
        log.info("Average: " + getAverage(pressureList));
        showMonth(pressureList);
        createCollector(pressureList);
    }
}