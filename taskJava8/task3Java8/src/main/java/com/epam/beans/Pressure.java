package com.epam.beans;
import java.time.LocalDate;

/**
 * The type Pressure.
 * @author Stepan Tvardovski
 */
public class Pressure {
    private int value;
    private LocalDate localDate;

    /**
     * Instantiates a new Pressure.
     *
     * @param value     the value of pressure
     * @param localDate the local date
     */
    public Pressure(int value, LocalDate localDate) {
        this.value = value;
        this.localDate = localDate;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(int value) {
        this.value = value;
    }

    /**
     * Gets local date.
     *
     * @return the local date
     */
    public LocalDate getLocalDate() {
        return localDate;
    }

    /**
     * Sets local date.
     *
     * @param localDate the local date
     */
    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    /**
     * toString method
     * @return object like string
     */
    @Override
    public String toString() {
        return "Pressure{" + "value=" + value + ", localDate=" + localDate + '}';
    }
}
