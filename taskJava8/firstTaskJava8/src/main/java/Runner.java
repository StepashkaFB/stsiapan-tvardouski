import com.epam.beans.Company;
import com.epam.beans.Phone;
import com.epam.beans.TripleCustom;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.function.*;
import java.util.stream.Collectors;

import static com.epam.constans.RunnerConstant.*;

/**
 * @author Stepan Tvardovski
 */
public class Runner {

    /**
     * logger
     */
    private static Logger log = Logger.getLogger(Runner.class.getName());
    /**
     * list of phones
     */
    private static List<Phone> phones = new ArrayList<>();
    
    /**
     * This supplier creates a new instance
     * of Company
     */
    private static Supplier<Company> companySupplier = () -> {
        Scanner in = new Scanner(System.in);
        log.info(ENTER_A_NAME_OF_COMPANY);
        String company = in.nextLine();
        log.info(ENTER_A_NAME_OF_CITY);
        String city = in.nextLine();
        return new Company(company, city);
    };
    
    /**
     * This consumer creates a csv string
     * from company object
     */
    private static Consumer<Company> companyConsumerCSV = (company) ->
            log.info(company.getName().concat(STR).concat(company.getCity()));
    /**
     * This unary operator use a phone object
     * for reduce cost of phone on 10%
     */
    private static UnaryOperator<Phone> phoneReduceCostOn10 = phone -> {
        int cost = (int) (phone.getCost() * 0.9);
        phone.setCost(cost);
        return phone;
    };
    /**
     * This binary operator getting two object of
     * phone and then return a phone with the most less
     * cost
     */
    private static BinaryOperator<Phone> phoneBinaryOperator = (phone, phone2) ->
            phone.getCost() > phone2.getCost() ? phone2 : phone;
    
    /**
     * This predicate checking that
     * phone's name is equal like huawei
     */
    private static Predicate<Phone> phonePredicate
            = phone -> phone.getCompany().getName().equals(NAME_COMPANY);
    /**
     * This Function getting a company
     * and then return a new phone
     */
    private static Function<Company, Phone> createEmptyPhone
            = p -> new Phone("", 0, p);

    /**
     * Demonstrate a logic
     * @param args - input args
     */
    public static void main(String[] args) {
        fillList();
        phones.removeIf(phonePredicate);
        Company company = companySupplier.get();
        companyConsumerCSV.accept(company);
        phoneReduceCostOn10.apply(phones.get(1));
        log.info(phoneBinaryOperator.apply(phones.get(0), phones.get(1)));
        log.info(company);
        sorting();
        Phone phone = createEmptyPhone.apply(new Company("apple","usa"));
        log.info(TripleCustom.isNull(phone.getName()));
        workWithFuncInt(1, -1, 1);
    }

    /**
     * This method describe a logic of
     *  filling a list of phones
     */
    private static void fillList() {
        List<String[]> lines = null;
        try {
            lines = Files.lines(Paths.get(Runner.class.getResource(NAME_FILE)
                    .toURI()))
                    .map(l -> l.split(SPACE))
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            log.info(e.getMessage());
        }
        if (lines != null) {
            lines.forEach(e -> phones.add(new Phone(e[0], Integer.parseInt(e[1])
                    , new Company(e[2], e[3]))));
        }

    }

    /**
     * This method describe a logic of 2 type
     *  of sorting list of phones
     */
    private static void sorting() {
        log.info(FIRST_SORT);
        phones.stream()
                .sorted(Comparator.comparing(e -> e.getCompany().getCity()))
                .forEach(log::info);
        log.info(SECOND_SORT);
        phones.stream()
                .sorted(Comparator.comparing(Phone::getName).thenComparingInt(Phone::getCost))
                .forEach(log::info);

    }

    /**
     * This method describe 3 different implementation
     *  of custom functional interface
     *
     * @param val1 - first value
     * @param val2 - second value
     * @param val3 - third value
     */
    private static void workWithFuncInt(int val1, int val2, int val3) {
        TripleCustom<Boolean, Integer> trianglePredicat =
                (a, b, c) -> a + b > c && a + c > b && b + c > a;
        log.info(TRIANGLE_IS_EXIST + trianglePredicat.test(val1, val2, val3));

        TripleCustom<Integer, Integer> tripleCustom = (a, b, c) -> a + b + c;
        log.info(SUM_NUMBERS + tripleCustom.test(val1, val2, val3));

        TripleCustom<Boolean, Integer> triplePredicat =
                (a, b, c) -> a > 0 && b > 0 && c > 0;
        log.info(ALL_NUMBERS_ARE_POSITIVE + triplePredicat.test(val1, val2, val3));
    }
}
