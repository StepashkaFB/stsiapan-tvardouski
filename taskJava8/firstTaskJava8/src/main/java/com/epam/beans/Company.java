package com.epam.beans;

/**
 * The type Company.
 */
public class Company {
    private String name;
    private String city;

    /**
     * Instantiates a new Company.
     *
     * @param name the name of company
     * @param city the city of company
     */
    public Company(String name, String city) {
        this.name = name;
        this.city = city;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
