package com.epam.beans;

/**
 * The type Phone.
 */
public class Phone {
    private String name;
    private int cost;
    private Company company;

    /**
     * Instantiates a new Phone.
     *
     * @param name    the name of phone
     * @param cost    the cost of phone
     * @param company the company of phone
     */
    public Phone(String name, int cost, Company company) {
        this.name = name;
        this.cost = cost;
        this.company = company;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets cost.
     *
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * Sets cost.
     *
     * @param cost the cost
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * Gets company.
     *
     * @return the company
     */
    public Company getCompany() {
        return company;
    }

    /**
     * Sets company.
     *
     * @param company the company
     */
    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", company=" + company +
                '}';
    }
}
