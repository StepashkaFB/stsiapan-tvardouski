package com.epam.beans;

/**
 * The interface Triple custom.
 *
 * @param <T> the type parameter
 * @param <R> the type parameter
 */
@FunctionalInterface
public interface TripleCustom<T, R> {
    /**
     * Test t.
     *
     * @param val1 the first value
     * @param val2 the second value
     * @param val3 the third value
     * @return the t
     */
    T test(R val1, R val2, R val3);

    /**
     * Gets type.
     *
     * @param t the t
     * @param r the r
     */
    default void getType(T t, R r) {
        System.out.println("First input type: " + t.getClass().getTypeName());
        System.out.println("Second input type: " + r.getClass().getTypeName());
    }

    /**
     * Is null boolean.
     *
     * @param str the str
     * @return the boolean
     */
    static boolean isNull(String str) {
        System.out.println("Checking if str is null: ");
        return str == null || ("".equals(str));
    }
}
