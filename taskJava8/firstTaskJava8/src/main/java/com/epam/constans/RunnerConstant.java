package com.epam.constans;

/**
 * This class contains constants for Runner class
 * @author Stepan Tvardovski
 */
public class RunnerConstant {
    public static final String ENTER_A_NAME_OF_COMPANY = "Enter a name of company: ";
    public static final String ENTER_A_NAME_OF_CITY = "Enter a name of city: ";
    public static final String STR = ";";
    public static final String NAME_COMPANY = "huawei";
    public static final String SPACE = " ";
    public static final String NAME_FILE = "phone.txt";
    public static final String FIRST_SORT = "First sort:";
    public static final String SECOND_SORT = "Second sort";
    public static final String TRIANGLE_IS_EXIST = "Triangle is exist: ";
    public static final String SUM_NUMBERS = "Sum numbers = ";
    public static final String ALL_NUMBERS_ARE_POSITIVE = "All numbers are positive: ";
}
