import com.epam.beans.Author;
import com.epam.beans.Book;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

import static com.epam.constants.RunnerConstant.*;
import static java.util.stream.Collectors.toList;

/**
 * @author Stepan Tvardovski
 */
public class Runner {

    private static Logger log = Logger.getLogger(Runner.class.getName());
    private static final int START_RANGE = 0;
    private static final int END_RANGE = 3;

    /**
     * Demonstration of completed tasks for java8
     * @param args - string args
     */
    public static void main(String[] args) {
        List<Author> authors = new ArrayList<>();
        fillAuthors(authors);
        List<Book> books = new ArrayList<>();
        fillBooks(books);

        // Random binding
        IntStream.rangeClosed(0, 7).forEach(e -> {
            for (int i = 0; i < ThreadLocalRandom.current().nextInt(START_RANGE, END_RANGE); i++) {
                authors.get(e).addBook(books.get(ThreadLocalRandom.current().nextInt(0, authors.size())));
                books.get(e).addAuthor(authors.get(ThreadLocalRandom.current().nextInt(0, books.size())));
            }
        });

        //1 task
        log.info(CHECK_IF_THERE_ARE_BOOKS_WITH_MORE_THAN_200_PAGES
                + books.stream().anyMatch(e -> e.getAmountOfPage() > 200));
        //2 task
        log.info(CHECK_IF_ALL_BOOKS_HAVE_MORE_THAN_200_PAGES
        + books.stream().allMatch(e -> e.getAmountOfPage() > 200));
        //3 task
        log.info(FIND_BOOKS_WITH_THE_LARGEST_AND_LEAST_NUMBER_OF_PAGES);
        books.stream()
                .filter(e-> (e.getAmountOfPage() ==
                        books.stream()
                                .max(Comparator.comparing(Book::getAmountOfPage))
                                .orElseThrow(RuntimeException::new)
                                .getAmountOfPage())
                        || (e.getAmountOfPage() ==
                        books.stream()
                                .min(Comparator.comparing(Book::getAmountOfPage))
                                .orElseThrow(RuntimeException::new)
                                .getAmountOfPage()))
                .forEach(log::info);
        //4 task
        log.info(FOUND_BOOKS_WITH_ONE_AUTHOR);
        books.stream()
                .filter(e->e.getAuthorList().size()==1)
                .forEach(log::info);
        //5 task
        log.info(SORT_BOOKS_BY_NUMBER_OF_PAGES_AND_THEN_BY_TITLE);
        books.stream()
                .sorted(Comparator.comparingInt(Book::getAmountOfPage)
                .thenComparing(Book::getNameOfBook))
                .forEach(log::info);
        //6 task
        log.info(GET_A_LIST_OF_ALL_UNIQUE_BOOK_TITLES);
        books.stream()
                .map(Book::getNameOfBook)
                .distinct()
                .forEach(log::info);
        //7 task
        log.info(GET_A_UNIQUE_LIST_OF_BOOK_AUTHORS_WITH_LESS_THAN_200_PAGES);
        books.stream()
                .filter(e->e.getAmountOfPage()<200)
                .map(Book::getAuthorList)
                .flatMap(Collection::stream)
                .map(Author::getNameAuthor)
                .distinct()
                .forEach(System.out::println);
    }

    /**
     * This method fills a list of authors
     * @param authors - list of authors
     */
    private static void fillAuthors(List<Author> authors) {
        List<String[]> lines = getStrings(AUTHOR_FILE);
        if (lines != null) {
            lines.forEach(e -> authors.add(new Author(e[0], Integer.parseInt(e[1]))));
        }
    }

    /**
     * This method fills a list of books
     * @param books - list of books
     */
    private static void fillBooks(List<Book> books) {
        List<String[]> lines = getStrings(BOOK_FILE);
        if (lines != null) {
            lines.forEach(e -> books.add(new Book(e[0], Integer.parseInt(e[1]))));
        }
    }

    /**
     * This method reads the file
     * @param s - name of file
     * @return - list of array of strings from file
     */
    private static List<String[]> getStrings(String s) {
        try {
            return Files.lines(Paths.get(Runner.class.getResource(s)
                    .toURI()))
                    .map(l -> l.split(REGEX))
                    .collect(toList());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
