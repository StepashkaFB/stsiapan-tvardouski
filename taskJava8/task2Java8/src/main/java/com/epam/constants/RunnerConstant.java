package com.epam.constants;

/**
 * This class contains constants for Runner class
 * @author Stepan Tvardovski
 */
public class RunnerConstant {
    public static final String CHECK_IF_THERE_ARE_BOOKS_WITH_MORE_THAN_200_PAGES = "Check if there are books with more than 200 pages: ";
    public static final String CHECK_IF_ALL_BOOKS_HAVE_MORE_THAN_200_PAGES = "Check if all books have more than 200 pages:";
    public static final String FIND_BOOKS_WITH_THE_LARGEST_AND_LEAST_NUMBER_OF_PAGES = "Find books with the largest and least number of pages";
    public static final String FOUND_BOOKS_WITH_ONE_AUTHOR = "Found books with one author";
    public static final String SORT_BOOKS_BY_NUMBER_OF_PAGES_AND_THEN_BY_TITLE = "Sort books by number of pages, and then by title";
    public static final String GET_A_LIST_OF_ALL_UNIQUE_BOOK_TITLES = "Get a list of all unique book titles";
    public static final String GET_A_UNIQUE_LIST_OF_BOOK_AUTHORS_WITH_LESS_THAN_200_PAGES = "Get a unique list of book authors with less than 200 pages";
    public static final String AUTHOR_FILE = "author.txt";
    public static final String BOOK_FILE = "book.txt";
    public static final String REGEX = ";";
}
