package com.epam.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Book.
 * @author Stepan Tvardovski
 */
public class Book {
    private String nameOfBook;
    private int amountOfPage;
    private List<Author> authorList = new ArrayList<>();
    private int counter;

    /**
     * Instantiates a new Book.
     *
     * @param nameOfBook   the name of book
     * @param amountOfPage the amount of page
     */
    public Book(String nameOfBook, int amountOfPage) {
        this.nameOfBook = nameOfBook;
        this.amountOfPage = amountOfPage;
    }

    /**
     * Instantiates a new Book.
     *
     * @param nameOfBook   the name of book
     * @param amountOfPage the amount of page
     * @param authorList   the author list
     */
    public Book(String nameOfBook, int amountOfPage, List<Author> authorList) {
        this(nameOfBook, amountOfPage);
        this.authorList = authorList;
    }

    /**
     * Gets name of book.
     *
     * @return the name of book
     */
    public String getNameOfBook() {
        return nameOfBook;
    }

    /**
     * Sets name of book.
     *
     * @param nameOfBook the name of book
     */
    public void setNameOfBook(String nameOfBook) {
        this.nameOfBook = nameOfBook;
    }

    /**
     * Gets amount of page.
     *
     * @return the amount of page
     */
    public int getAmountOfPage() {
        return amountOfPage;
    }

    /**
     * Sets amount of page.
     *
     * @param amountOfPage the amount of page
     */
    public void setAmountOfPage(int amountOfPage) {
        this.amountOfPage = amountOfPage;
    }

    /**
     * Gets author list.
     *
     * @return the author list
     */
    public List<Author> getAuthorList() {
        return authorList;
    }

    /**
     * Sets author list.
     *
     * @param authorList the author list
     */
    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    /**
     * Add author.
     *
     * @param author the author
     */
    public void addAuthor(Author author) {
        authorList.add(author);
    }
    /**
     * Modified toString to avoid a stack overflow
     * @return - object like string
     */
    @Override
    public String toString() {
        if (counter != 0) {
            return "Book{" +
                    "nameOfBook='" + nameOfBook + '\'' +
                    ", amountOfPage=" + amountOfPage +
                    '}';
        }
        counter++;
        return "Book{" +
                "nameOfBook='" + nameOfBook + '\'' +
                ", amountOfPage=" + amountOfPage +
                ", authorList=" + authorList +
                '}';
    }
}
