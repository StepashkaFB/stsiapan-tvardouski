package com.epam.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Author.
 * @author Stepan Tvardovski
 */
public class Author {
    private String nameAuthor;
    private int age;
    private List<Book> bookList = new ArrayList<>();
    private int counter;

    /**
     * Instantiates a new Author.
     *
     * @param nameAuthor the name author
     * @param age        the age of author
     */
    public Author(String nameAuthor, int age){
        this.nameAuthor = nameAuthor;
        this.age = age;
    }

    /**
     * Instantiates a new Author.
     *
     * @param nameAuthor the name author
     * @param age        the age of author
     * @param bookList   the book list
     */
    public Author(String nameAuthor, int age, List<Book> bookList){
        this(nameAuthor, age);
        this.bookList = bookList;
    }

    /**
     * Gets name author.
     *
     * @return the name author
     */
    public String getNameAuthor() {
        return nameAuthor;
    }

    /**
     * Sets name author.
     *
     * @param nameAuthor the name author
     */
    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    /**
     * Gets age.
     *
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * Sets age.
     *
     * @param age the age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Gets book list.
     *
     * @return the book list
     */
    public List<Book> getBookList() {
        return bookList;
    }

    /**
     * Sets book list.
     *
     * @param bookList the book list
     */
    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    /**
     * Add book.
     *
     * @param book the book
     */
    public void addBook(Book book){
        bookList.add(book);
    }

    /**
     * Modified toString to avoid a stack overflow
     * @return - object like string
     */
    @Override
    public String toString() {
        if(counter != 0){
            return "Author{" +
                    "nameAuthor='" + nameAuthor + '\'' +
                    ", age=" + age +
                    '}';

        }
        counter++;
        return "Author{" +
                "nameAuthor='" + nameAuthor + '\'' +
                ", age=" + age +
                ", bookList=" + bookList +
                '}';
    }
}
