package main.java.by.epamlab.exceptions;

/**
 * Class DataIncorrectException describes user exception
 * - extends RuntimeException
 * @author Stsiapan Tvardouski
 */
public class DataIncorrectException extends RuntimeException {
    /**
     * This field contains information about error
     */
    public static final String INCORRECT_DATA = "Incorrect data: ";

    /**
     * Constructor
     * @param value - wrong number
     */
    public DataIncorrectException(int value) {
        super(INCORRECT_DATA + value);
    }

}
