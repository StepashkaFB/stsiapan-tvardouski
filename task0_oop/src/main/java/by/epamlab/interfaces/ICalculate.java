package main.java.by.epamlab.interfaces;

public interface ICalculate{
    boolean checkToMove(int value);
    int getSpeed();
}
