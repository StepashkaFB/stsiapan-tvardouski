package main.java.by.epamlab.interfaces;

public interface IBug {
    void runUp();
    void shift(int distance);
    void stop();
    int powerConsumption(); //speed * factor + mass

}
