package main.java.by.epamlab.interfaces;

public interface IMovement {
    int getMass();
    int getPower();
}
