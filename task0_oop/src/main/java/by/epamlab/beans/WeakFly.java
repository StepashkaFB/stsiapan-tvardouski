package main.java.by.epamlab.beans;

import main.java.by.epamlab.interfaces.ICalculate;
import main.java.by.epamlab.interfaces.IMovement;

/**
 * Class WeakFly describes weak fly with limited capabilities
 * - extends SimpleFly
 * - implements IBug
 * @author Stsiapan Tvardouski
 */
public class WeakFly extends SimpleFly implements ICalculate {
    /**
     * This field contains information about max power, which fly could spent
     */
    private static final int MAX_POWER = 5000;
    /**
     * This field contains information about max speed, which fly could reach
     */
    private static final int MAX_SPEED = 250;
    /**
     * This field using for console output
     */
    public static final String CHECK_FAILED = "Check failed!, please choose another distance! Incorrect distance: ";

    /**
     * Constructor
     * @param name - name of bug
     * @param mass - mass of bug
     * @param wing - type of wing
     */
    public WeakFly(String name, int mass, IMovement wing) {
        super(name, mass, wing);
    }

    /**
     * This method describes check logic
     * @param distance - path that a fly must pass
     * @return - result of check
     */
    @Override
    public boolean checkToMove(int distance) {
        return MAX_POWER >= distance*powerConsumption() && MAX_SPEED >=getSpeed();
    }

    /**
     * This method describes logic of counting speed
     * @return - speed of fly
     */
    @Override
    public int getSpeed() {
        return getWing().getPower() / getRatioMass();
    }

    /**
     * This method describes logic of shift of fly
     * @param distance - path that a fly must pass
     */
    @Override
    public void shift(int distance) {
        if(checkToMove(distance)){

            super.shift(distance);
        }else{
            System.out.println(CHECK_FAILED + distance);
        }
    }
}
