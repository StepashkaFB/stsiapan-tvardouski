package main.java.by.epamlab.beans;

import main.java.by.epamlab.exceptions.DataIncorrectException;
import main.java.by.epamlab.interfaces.ICalculate;
import main.java.by.epamlab.interfaces.IMovement;

/**
 * Class ElectricFly describes electric fly with limited power
 * - extends SimpleFly
 * - implements ICalculate
 * @author Stsiapan Tvardouski
 */
public class ElectricFly extends SimpleFly implements ICalculate {
    /**
     * This field using for console output
     */
    public static final String CURRENT_ENERGY = "Enough energy to fly, current energy = ";
    /**
     * This field using for console output
     */
    public static final String SPEED = " with speed: ";
    /**
     * This field using for console output
     */
    public static final String DISTANCE_TRAVELED = "Distance traveled: ";
    /**
     * This field using for console output
     */
    public static final String ENERGY_RESIDUE = "Energy residue: ";
    /**
     * This field using for console output
     */
    public static final String NOT_ENOUGH_ENERGY_TO_FLY_OR_TURN_ON_THE_ELECTRIC_MOTOR = "Not enough energy to fly or turn on the electric motor";
    /**
     * This field using for console output
     */
    public static final String POWER_RESERVE = "Power reserve: ";
    /**
     * This field using for console output
     */
    public static final String BALANCE_POWER = "Balance power: ";
    /**
     * This field contains information about power reserve of fly
     */
    private int powerReserve;
    /**
     * This field contains information about current power reserve
     */
    private int balancePowerReserve;
    /**
     * Constructor
     * @param name - name of bug
     * @param mass - mass of bug
     * @param powerReserve - power reserve of fly
     * @param wing - type of wing
     */
    public ElectricFly(String name, int mass,int powerReserve, IMovement wing) {
        super(name, mass, wing);
        setPowerReserve(powerReserve);
        balancePowerReserve = powerReserve;
    }

    /**
     * setter with check logic {@link ElectricFly#powerReserve}
     * @param powerReserve - power reserve of fly
     */
    public void setPowerReserve(int powerReserve) {
        if(powerReserve <= 0){
            throw  new DataIncorrectException(powerReserve);
        }
        this.powerReserve=powerReserve;
    }
    /**
     *This method describes start logic of fly
     */
    @Override
    public void runUp(){
        setReadyToMove(powerReserve>=powerConsumption());
    }
    /**
     * This method describes check logic
     * @param energy - necessary energy
     * @return - result of check
     */
    @Override
    public boolean checkToMove(int energy) {
        return powerReserve > energy;
    }
    /**
     * This method describes logic of shift of fly
     * @param distance - path that a fly must pass
     */
    @Override
    public void shift(int distance) {
        int temp = powerConsumption()*distance;
        if(checkToMove(temp) && isReadyToMove()){
            System.out.println(CURRENT_ENERGY + balancePowerReserve);
            setAmountOfEnergy(temp);
            setDistance(distance);
            balancePowerReserve -= temp;
            System.out.println(ENERGY_EXPENDED + temp);
            System.out.println(DISTANCE_TRAVELED + distance + SPEED + getSpeed());
            System.out.println(ENERGY_RESIDUE + balancePowerReserve);
        }else {
            System.out.println(NOT_ENOUGH_ENERGY_TO_FLY_OR_TURN_ON_THE_ELECTRIC_MOTOR);
        }
    }

    /**
     * This method describes logic of getting speed
     * @return - speed of fly
     */
    @Override
    public int getSpeed() {
        return getWing().getPower() / getRatioMass();
    }

    /**
     * Representation of the object as a string
     * @return object state
     */
    @Override
    public String toString(){
        return new StringBuilder(super.toString()).append(COMMA).append(STR).append(POWER_RESERVE).append(powerReserve)
                .append(COMMA).append(BALANCE_POWER).append(balancePowerReserve).toString();
    }
}
