package main.java.by.epamlab.beans;

import main.java.by.epamlab.exceptions.DataIncorrectException;
import main.java.by.epamlab.interfaces.IMovement;

import static main.java.by.epamlab.beans.AbstractBug.*;

/**
 * Class Wing describes model of wing of bug
 * - implements IMovement
 * @author Stsiapan Tvardouski
 */
public class Wing implements IMovement {
    /**
     * This field using for console output
     */
    public static final String NAME_OF_WING = "Name of wing: ";
    /**
     * This field using for console output
     */
    public static final String FREQUENCY_OF_WING = "Frequency of wing: ";
    /**
     * This field using for console output
     */
    public static final String NUMBER_OF_WING = "Number of wing: ";
    /**
     * This field contains information about name of wing
     */
    private final String name;
    /**
     * This field contains information about frequency of wings
     */
    private int frequency;
    /**
     * This field contains information about number of wings
     */
    private int amountOfWing;
    /**
     * This field contains information about mass of wing
     */
    private int mass;
    /**
     * Constructor
     *  @param frequency - frequency of wings
     *  @param amountOfWing - number of wings
     */
    public Wing(String name, int frequency, int amountOfWing, int mass) {
        this.name = name;
        setFrequency(frequency);
        setAmountOfWing(amountOfWing);
        setMass(mass);
    }

    /**
     * This method describes mass counting logic
     * @return mass of wing
     */
    @Override
    public int getMass(){
        return amountOfWing * mass;
    }

    /**
     * This method describes logic of getting power of wing
     * @return power of wing
     */
    @Override
    public int getPower() {
        return (frequency*amountOfWing)/mass;
    }

    /**
     * setter with check logic {@link Wing#frequency}
     * @param frequency - frequency of wings
     */
    public void setFrequency(int frequency) {
        if(frequency<1){
            throw new DataIncorrectException(frequency);
        }
        this.frequency = frequency;
    }

    /**
     * setter with check logic {@link Wing#amountOfWing}
     * @param amountOfWing - number of wings of a fly
     */
    public void setAmountOfWing(int amountOfWing) {
        if(amountOfWing<1){
            throw new DataIncorrectException(amountOfWing);
        }
        this.amountOfWing = amountOfWing;
    }

    /**
     * setter with check logic {@link Wing#mass}
     * @param mass - masses of a fly
     */
    public void setMass(int mass) {
        if(mass <= 0) {
            throw new DataIncorrectException(mass);
        }
        this.mass = mass;
    }

    /**
     * Representation of the object as a string
     * @return object state
     */
    @Override
    public String toString() {
        return new StringBuilder().append(NAME_OF_WING).append(name).append(COMMA).append(FREQUENCY_OF_WING).append(frequency)
                .append(COMMA).append(NUMBER_OF_WING).append(amountOfWing).toString();
    }
}
